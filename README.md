**Expense Website Application**

The Expense Website manages expenses for employees who need reimbursement approval from managers.  The user has the ability to login and create expenses and change profile information.  The manager has the functionality to accept or reject expenses.  The manager can view and change users.

**Features:**

An Employee can:

- Login
- View the employee home page
- View his expenses.
- View his profile.
- Change his profile.
- Add an expense.
- View all reimbursements.
- View approved reimbursements.
- View pending reimbursements.
- View rejected reimbursements.


An Admin can:

- Login
- View the admin home page.
- Add a new employee.
- Change employee information.
- Approve an expense from a list of pending expenses.
- Reject an expense from a list of pending expenses.
- View all reimbursements for all or a single employee.
- View approved reimbursements for all or a single employee.
- View pending reimbursements for all or a single employee.
- View rejected reimbursements for all or a single employee.


Technologies Used:

- Java 1.8
- Postgres
- SQL
- JavaScript
- HTML
- CSS


Deployment:

To run from source code:

• git clone `https://gitlab.com/lalkapeter123/project1.git`


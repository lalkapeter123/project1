import java.sql.SQLException;

import Factories.TableFactory;

import junit.framework.Assert;



public class TableFactoryTest {
	
	public void testTableFactoryTest() throws SQLException {
		
		
		TableFactory tablefactory = new TableFactory();
		
		Assert.assertNotSame(tablefactory.createEmployeeApprovedClaimsTable("1").length(),168280);
		Assert.assertNotSame(tablefactory.createEmployeePendingClaimsTable("1").length(),7174);
		Assert.assertNotSame(tablefactory.createEmployeeReimbursementClaimsTable("1").length(),73657);
		Assert.assertNotSame(tablefactory.createEmployeeRejectedClaimsTable("1").length(),1529);
		Assert.assertNotSame(tablefactory.createEmployeeTableView().length(),0);
		Assert.assertNotSame(tablefactory.createEmployeeTable().length(),0);
		
		Assert.assertNotSame(tablefactory.createPendingClaimsTable().length(),22749);
		Assert.assertNotSame(tablefactory.createReimbursementClaimsTable().length(),220491);
		Assert.assertNotSame(tablefactory.createRejectedClaimsTable().length(),4136);
		Assert.assertNotSame(tablefactory.getEmployeesTableHeader().length(),970);
		
	}

}

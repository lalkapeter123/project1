import java.util.ArrayList;
import java.util.List;

import javax.servlet.GenericServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import junit.framework.Assert;


public class EmployeeViewServletTest {

	public void testEmployeeViewServletTest() {
		
		//HttpServletRequest request = new GenericServlet();
		//HttpServletResponse response;
		
		//request.setAttribute("1", "id");
		
		//request.setAttribute(null, response);
		
		EmployeeViewServlet employeeviewservlet = new EmployeeViewServlet();
		Assert.assertFalse(employeeviewservlet == null);
		employeeviewservlet.init();
		
		Assert.assertFalse(employeeviewservlet.parameterhandler == null);
		Assert.assertFalse(employeeviewservlet.webpagefactory == null);
		Assert.assertFalse(employeeviewservlet.arrlststrs == null);
		
	}
	
}

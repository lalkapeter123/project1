//Change the id in the calls to the database to a parameter or session id variable.

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DatabaseCommands.DatabaseService;
import Factories.TableFactory;
import Factories.WebPageFactory;


public class ViewReimbursementsServlet extends HttpServlet {
	
	TableFactory tablefactory = new TableFactory();
	WebPageFactory webpagefactory = new WebPageFactory();
	
	String stremployeeclaimsheader = webpagefactory.createEmployeeHeader();
	String stremployeeclaimsfooter = "</body>\r\n</html>";
	
	public void init() {}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	    
		//if there is a valid session id in the current user object
		if(Data.CurrentUser.intsessionid != -1) {

			//EMPLOYEE CLAIMS VIEW
		    String strrequesturl = request.getRequestURL().toString();
		      
			if(strrequesturl.endsWith("EmployeeReimbursementClaims.html")) {
	
				try {
					//get the employee claims records
					String strrecords = "<br /><span style=\"font-family:sans-serif\">All Claims</span><br /><br />\r\n"; 
					strrecords += tablefactory.createEmployeeReimbursementClaimsTable(String.valueOf(Data.CurrentUser.intuserid)); 
					//send the records to the client
					printHTML(stremployeeclaimsheader + strrecords + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	
			//Employee Pending Claims
			} else if(strrequesturl.endsWith("EmployeePendingReimbursements.html")) {
				try {
					//get the employee pending claims records
					String strrecords = "<br /><span style=\"font-family:sans-serif\">Pending Claims</span><br /><br />\r\n"; 
					strrecords += tablefactory.createEmployeePendingClaimsTable(String.valueOf(Data.CurrentUser.intuserid)); 
					//send the records to the client
					printHTML(stremployeeclaimsheader + strrecords + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	
			//Employee Approved Claims			
			} else if(strrequesturl.endsWith("EmployeeApprovedReimbursements.html")) {
				
				try {
					//get the employee pending claims records
					 String strrecords = "<br /><span style=\"font-family:sans-serif\">Approved Claims</span><br /><br />\r\n";
					 strrecords += tablefactory.createEmployeeApprovedClaimsTable(String.valueOf(Data.CurrentUser.intuserid));
					//send the records to the client
					printHTML(stremployeeclaimsheader + strrecords + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	
			//Employee Rejected Claims
			} else if(strrequesturl.endsWith("EmployeeRejectedReimbursements.html")) {	
		
				try {
					//get the employee pending claims records
					String strrecords = "<br /><span style=\"font-family:sans-serif\">Rejected Claims</span><br /><br />\r\n";
					strrecords += tablefactory.createEmployeeRejectedClaimsTable(String.valueOf(Data.CurrentUser.intuserid));
					//send the records to the client
					printHTML(stremployeeclaimsheader + strrecords + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			
			//MANAGER CLAIMS VIEW
	
			//All Claims
			} else if(strrequesturl.endsWith("ManagerReimbursementClaims.html")) {

				try {
					//get the employee pending claims records
					String strmanagerwebpageheader = webpagefactory.createManagerWebPageHeader();
					String strchoice = "<br /><br />" + webpagefactory.getEmployeeChoiceForm();
					String strclaims = "<span style=\"font-family:sans-serif\">All Claims</span><br /><br />";
					strclaims += tablefactory.createReimbursementClaimsTable();
					//send the records to the client
					printHTML(strmanagerwebpageheader + strchoice + strclaims + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}

			//Pending Claims
			} else if(strrequesturl.endsWith("ManagerPendingClaims.html")) {

				try {
					//get the employee pending claims records
					String strmanagerwebpageheader = webpagefactory.createManagerWebPageHeader();
					String strchoice = "<br /><br />" + webpagefactory.getEmployeeChoiceForm();
					String strclaims = "<span style=\"font-family:sans-serif\">Pending Claims</span><br /><br />";
					strclaims += tablefactory.createPendingClaimsTable();
					//send the records to the client
					printHTML(strmanagerwebpageheader + strchoice + strclaims + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			
			//Approved Claims
			} else if(strrequesturl.endsWith("ManagerApprovedClaims.html")) {
							
				try {
					//get the employee pending claims records
					String strmanagerwebpageheader = webpagefactory.createManagerWebPageHeader();
					String strchoice = "<br /><br />" + webpagefactory.getEmployeeChoiceForm();
					String strclaims = "<span style=\"font-family:sans-serif\">Approved Claims</span><br /><br />";
					strclaims += tablefactory.createApprovedClaimsTable("-1");
					//send the records to the client
					printHTML(strmanagerwebpageheader + strchoice + strclaims + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	
			//Rejected Claims
			} else if(strrequesturl.endsWith("ManagerRejectedClaims.html")) {			
							
				try {
					//get the employee pending claims records
					String strmanagerwebpageheader = webpagefactory.createManagerWebPageHeader();
					String strchoice = "<br /><br />" + webpagefactory.getEmployeeChoiceForm();
					String strclaims = "<span style=\"font-family:sans-serif\">Rejected Claims</span><br /><br />";
					strclaims += tablefactory.createRejectedClaimsTable();
					//send the records to the client
					printHTML(strmanagerwebpageheader + strchoice + strclaims + stremployeeclaimsfooter, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				
			}
			
		//if there is not a valid session id in the current user object
		} else {
			
			String strremoteaddress = request.getRemoteAddr();
	        String strwebpage = request.getHeader("referer");
	        
			redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/EmployeeLogin.html", response);
			
		}
		
	}
	
	
	//POST
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	    String strrequesturl = request.getRequestURL().toString();
	
	    //all
	    //the user entered a value for employee to view all reimbursement claims
		if(strrequesturl.endsWith("ManagerReimbursementClaims.html")) {
			
			String strid = request.getParameter("employee");
			String strfullname = getEmployeeFullName(strid);
			
			try {
				
				String strHTML = "";
				
				if(strid.compareTo("-1") == 0) {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">All Claims</span><br /><br />";
					strHTML += tablefactory.createReimbursementClaimsTable();
					strHTML += "</body></html>";
				} else {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">All Claims for " + strfullname + "</span><br /><br />";
					strHTML += tablefactory.createEmployeeReimbursementClaimsTable(strid);
					strHTML += "</body></html>";
				}
				
				printHTML(strHTML, response);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}

	    //pending
		//the user entered a value for employee to view pending reimbursement claims
		} else if(strrequesturl.endsWith("ManagerPendingClaims.html")) {
			
			String strid = request.getParameter("employee");
			String strfullname = getEmployeeFullName(strid);
			
			try {
				
				String strHTML = "";
				
				if(strid.compareTo("-1") == 0) {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">All Pending Claims</span><br /><br />";
					strHTML += tablefactory.createPendingClaimsTable();
					strHTML += "</body></html>";
				} else {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">Pending Claims for " + strfullname + "</span><br /><br />";
					strHTML += tablefactory.createEmployeePendingClaimsTable(strid);
					strHTML += "</body></html>";
				}
				
				printHTML(strHTML, response);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		
		//approved
		//the user entered a value for employee to view approved reimbursement claims
		} else if(strrequesturl.endsWith("ManagerApprovedClaims.html")) {
						
			String strid = request.getParameter("employee");
			String strfullname = getEmployeeFullName(strid);
			
			try {
				
				String strHTML = "";
				
				if(strfullname == null) {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">Approved Claims</span><br /><br />";
					strHTML += tablefactory.createApprovedClaimsTable("-1");
					strHTML += "</body></html>";
				} else {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">Approved Claims for " + strfullname + "</span><br /><br />";
					strHTML += tablefactory.createApprovedClaimsTable(strid);
					strHTML += "</body></html>";
				}
				
				printHTML(strHTML, response);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}

		//rejected
		//the user entered a value for employee to view rejected reimbursement claims
		} else if(strrequesturl.endsWith("ManagerRejectedClaims.html")) {			
						
			String strid = request.getParameter("employee");
			String strfullname = getEmployeeFullName(strid);
			
			try {
				
				String strHTML = "";
				
				if(strid.compareTo("-1") == 0) {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">Rejected Claims</span><br /><br />";
					strHTML += tablefactory.createRejectedClaimsTable();
					strHTML += "</body></html>";
				} else {
					strHTML = new WebPageFactory().createManagerWebPageHeader();
					strHTML += "<br /><br />" + new WebPageFactory().getEmployeeChoiceForm();
					strHTML += "<span style=\"font-family:sans-serif\">Rejected Claims for " + strfullname + "</span><br /><br />";
					strHTML += tablefactory.createEmployeeRejectedClaimsTable(strid);
					strHTML += "</body></html>";
				}
				
				printHTML(strHTML, response);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			
		}
	
	}
	
	
	
	public void destroy() {}
	
	
	public void printHTML(String strcontent, HttpServletResponse response) throws IOException {
	    response.setContentType("text/html");
	    PrintWriter printwriter=response.getWriter();
		printwriter.println(strcontent);				
		printwriter.close();
	}	
   
   
   //redirect to the user to a webpage
   public void redirectTo(String _strremoteaddress, String _strwebpage, String _strredirectto, HttpServletResponse _response) throws IOException {
	   _response.setHeader(_strremoteaddress, _strwebpage);
	   _response.setContentType("text/html");
	   _response.sendRedirect(_strredirectto);
   }
	
   
   //the purpose of this function is to return the full name of an employee for the reimbursement views given an id.
   public String getEmployeeFullName(String _strid) {
		
		String strfullname = null;
		
		try {
			ArrayList<Object> arrlstobjects = (ArrayList<Object>)new DatabaseService().getCommand("view employee",new String[]{_strid});
			strfullname = arrlstobjects.get(0) + " " + arrlstobjects.get(1);
		}catch(Exception e) {}
		
		return strfullname;
				
	}
	
}

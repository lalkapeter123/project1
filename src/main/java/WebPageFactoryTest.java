import java.sql.SQLException;

import Factories.WebPageFactory;
import junit.framework.Assert;

public class WebPageFactoryTest {
	
	public void testWebPageFactoryTest() throws SQLException {
		
		WebPageFactory webpagefactory = new WebPageFactory();
		
		Assert.assertNotSame(webpagefactory.createAddEmployeeWebPage().length(), 2006);
		Assert.assertNotSame(webpagefactory.createChangeEmployeeForm("1").length(), 6501);
		Assert.assertNotSame(webpagefactory.createEmployeeHeader().length(), 3182);
		Assert.assertNotSame(webpagefactory.createManagerWebPageHeader().length(), 4246);
		Assert.assertNotSame(webpagefactory.createViewEmployeeProfileWebPage("0").length(), 5532);		
		Assert.assertNotSame(webpagefactory.removedEmployee("1").length(), 3306);
		
		
	}

}

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import DatabaseCommands.DatabaseService;

//The purpose of this servlet is to direct get requests from the user to a blank logoff webpage.
public class LogoffServlet extends HttpServlet {
	
	public void init() {}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String strwebpage = request.getRequestURL().toString();
		String strwebpage1 = request.getHeader("referer");
		
		//System.out.println(strwebpage);
		//System.out.println(strwebpage1);
		
		//set the user variable to an empty string
		//the login servlet will check for the address and require a new login
		
		//Remove the session so the ip address will not automatically log the user in.
		try {

			//get the remote address
			String strremoteaddress = request.getRemoteAddr();
			//remove the remote address from the session list so the browser will not automatically authenticate
			new DatabaseService().getCommand("logoff employee",new Object[] {strremoteaddress});
			
			//reset the session and user id variables
			Data.CurrentUser.strremoteipaddress = "";
			Data.CurrentUser.intuserid = -1;
			Data.CurrentUser.intsessionid = -1;
			
			
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		//redirect to the blank employee or manager logoff page
		if(strwebpage1.endsWith("Employee.html") || strwebpage1.endsWith("employeelogin")) {
			response.sendRedirect("http://localhost:8080/ExpenseWebsite/Logoff/EmployeeLogoff.html");
		} else if(strwebpage1.endsWith("Manager.html") || strwebpage1.endsWith("managerlogin")) {
			response.sendRedirect("http://localhost:8080/ExpenseWebsite/Logoff/ManagerLogoff.html");
		}

	}
	
	//post
	//there are no post methods to this servlet
	public void doPost(HttpServletRequest request, HttpServletResponse response) {}
	
	public void destroy() {}

}

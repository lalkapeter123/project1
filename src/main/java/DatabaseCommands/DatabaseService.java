package DatabaseCommands;

import java.io.PrintWriter;
import java.sql.SQLException;

//database service
public class DatabaseService {
	
	//database command repository
	DatabaseCommandRepository databasecommandrepository = new DatabaseCommandRepository();
	
	//constructor
	public DatabaseService() {}
	
	//get command
	//parameters: the command type and an object that could be an array
	//returns: an object
	public Object getCommand(String _str, Object _object) throws SQLException, ArrayIndexOutOfBoundsException {

		String string = _str;
		Object o = new Object();
		
		//the switch statement for the command type
		switch(string) {
		
			//TRAFFIC
			case "add traffic":
				o= databasecommandrepository.addTraffic(((Object[])_object)[0].toString(),((Object[])_object)[1].toString());
				break;		

			//employee options
			case "employee options":
				o = databasecommandrepository.getEmployeeOptions();
				break;
				
			//USER
			//logon user
			case "logon user":
				//get a session id
				o = databasecommandrepository.logonUser(((Object[])_object)[0].toString(),((Object[])_object)[1].toString(),((Object[])_object)[2].toString());
				break;

			//logoff user
			case "logoff user":
				o = databasecommandrepository.logoffUser(((Object[])_object)[0].toString());
				break;
			
			//lock out user
			case "lock out user":
				o = databasecommandrepository.lockOutUser(((Object[])_object)[0].toString(),((Object[])_object)[1].toString()); 
				break;

			//user locked out
			case "user locked":
				o = databasecommandrepository.userLockedOut(((Object[])_object)[0].toString(),((Object[])_object)[1].toString()); 
				break;
					
			//add a claim
			case "add claim":
				o = databasecommandrepository.addClaim(((Object[])_object)[0].toString(),((Object[])_object)[1].toString(),((Object[])_object)[2].toString(),((Object[])_object)[3].toString(),((Object[])_object)[4].toString());
				break;
				
			//change a claim
			case "change a claim":
				break;
				
			//MANAGER
			//logon manager
			case "logon manager":
				o = databasecommandrepository.logonManager(((Object[])_object)[0].toString(),((Object[])_object)[1].toString(),((Object[])_object)[2].toString());
				break;
			//logoff manager
			case "logoff manager":
				o = Integer.valueOf(0);
				break;

			//manager locked out
			case "manager locked":
				o = databasecommandrepository.managerLockedOut(((Object[])_object)[0].toString(),((Object[])_object)[1].toString()); 
				break;
					
			//get employee address
			case "get employee address":
				o = databasecommandrepository.getEmployeeAddress((String)_object);
				break;

			//view all employees
			case "view all employees":
				o = databasecommandrepository.getAllEmployees();
				break;
				
			case "get user id":
				o = databasecommandrepository.getUserID(((Object[])_object)[0].toString(),((Object[])_object)[1].toString());				
				break;

			case "get manager id":
				o = databasecommandrepository.getManagerID(((Object[])_object)[0].toString(),((Object[])_object)[1].toString());				
				break;
				
			//claim management
			//approve a claim
			case "approve claim":
				o = databasecommandrepository.approveClaim(((Object[])_object)[0].toString(),((Object[])_object)[1].toString());
				break;
			//reject a claim
			case "reject claim":
				o = databasecommandrepository.rejectClaim(((Object[])_object)[0].toString(),((Object[])_object)[1].toString());
				break;
					

			//MANAGER CLAIM VIEWS
				
			//view all claims
			case "view all claims":
				o = databasecommandrepository.getAllClaims();
				break;
			//view all approved claims
			case "view approved claims":
				o = databasecommandrepository.getAllApprovedClaims(((Object[])_object)[0].toString());
				break;
			//view all pending claims
			case "view pending claims":
				o = databasecommandrepository.getAllPendingClaims();
				break;
			//view all rejected claims
			case "view rejected claims":
				o = databasecommandrepository.getAllRejectedClaims();
				break;


			//EMPLOYEE CLAIM VIEWS

			//view employee approved claims
			case "view employee approved claims":
				o = databasecommandrepository.getEmployeeApprovedClaims(((Object[])_object)[0].toString());
				break;
			//view employee pending claims
			case "view employee pending claims":
				o = databasecommandrepository.getEmployeePendingClaims(((Object[])_object)[0].toString());
				break;
			//view employee rejected claims
			case "view employee rejected claims":
				o = databasecommandrepository.getEmployeeRejectedClaims(((Object[])_object)[0].toString());
				break;
			//view employee claims
			case "view employee claims":
				o = databasecommandrepository.getAllClaims(((Object[])_object)[0].toString());
				break;
				
			//employee registration

			//add employee
			case "add employee":
				o = databasecommandrepository.addEmployee(((Object[])_object)[0].toString(),((Object[])_object)[1].toString(),((Object[])_object)[2].toString(),((Object[])_object)[3].toString(),((Object[])_object)[4].toString(),((Object[])_object)[5].toString(),((Object[])_object)[6].toString(),((Object[])_object)[7].toString(),((Object[])_object)[8].toString(),((Object[])_object)[9].toString(),((Object[])_object)[10].toString());
				break;		

			//remove employee
			case "remove employee":
				o = databasecommandrepository.removeEmployee((Integer)((Object[])_object)[0]);
				break;
				
			//change employee
			case "change employee":
				o = databasecommandrepository.updateEmployee(((Object[])_object)[0].toString(),((Object[])_object)[1].toString(),((Object[])_object)[2].toString(),((Object[])_object)[3].toString(),((Object[])_object)[4].toString(),((Object[])_object)[5].toString(),((Object[])_object)[6].toString(),((Object[])_object)[7].toString(),((Object[])_object)[8].toString(),((Object[])_object)[9].toString(),((Object[])_object)[10].toString(),((Object[])_object)[11].toString());
				break;	
				
			//employee management
			case "view employee":
				o = databasecommandrepository.getEmployee((String)((Object[])_object)[0]);
				break;

			//employee profile
			case "view employee profile":
				o = databasecommandrepository.getEmployeeProfile((String)((Object[])_object)[0]);
				break;
			
			default:
				break;
		
		}
		
		return o;
		
	}

}

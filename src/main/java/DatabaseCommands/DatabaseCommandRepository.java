package DatabaseCommands;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.//logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


//register the driver

//database command repository
public class DatabaseCommandRepository {

	//private static final //logger //logger = LogManager.get//logger(DatabaseCommandRepository.class);
	
	//connection variable
	Connection connection = null;
	//prepared variable
	PreparedStatement preparedstatement;
	

	//USER
	
	//add traffic
	String strAddTrafficPreparedStatement = "SELECT \"Reimbursement\".\"f_addTraffic\"(?,?)";	
	
	//user access
	String strUserLogonPreparedStatement = "SELECT \"Reimbursement\".\"f_employeeLogon\"(?,?)";
	String strUserLogoffPreparedStatement = "SELECT \"Reimbursement\".\"f_employeeLogoff\"(?)";
	String strLockOutUserPreparedStatement = "SELECT \"Reimbursement\".\"f_lockOutUser\"(?,?)";
	String strUserLockedPreparedStatement = "SELECT \"Reimbursement\".\"f_isLockedOut\"(?,?)";

	//manager logon
	String strGetUserIDPreparedStatement = "SELECT \"Reimbursement\".\"f_getUserID\"(?,?)";
	String strGetManagerIDPreparedStatement = "SELECT \"Reimbursement\".\"f_getManagerID\"(?,?)";
	String strManagerLogonPreparedStatement = "SELECT \"Reimbursement\".\"f_managerLogon\"(?,?)";
	String strLockOutManagerPreparedStatement = "SELECT \"Reimbursement\".\"f_lockOutManager\"(?,?)";
	String strManagerLockedPreparedStatement = "SELECT \"Reimbursement\".\"f_isLockedOut\"(?,?)";
	
	//add session
	String strAddSessionPreparedStatement = "SELECT \"Reimbursement\".\"f_addSession\"(?,?)";
	
	//set password
	String strSetPasswordPreparedStatement = "SELECT \"Reimbursement\".\"f_getSetPassword\"()";	
	//change password
	String strChangePasswordPreparedStatement = "SELECT \"Reimbursement\".\"f_getChangePassword\"()";
	
	//lock account
	String strLockAccountPreparedStatement = "SELECT \"Reimbursement\".\"f_lockAccount\"(?)";
	
	//add claim
	String strAddClaimPreparedStatement = "SELECT \"Reimbursement\".\"f_addClaim\"(?,?,?,?::Date,?::Real)";
	
	//get employee address
	String strGetEmployeeAddressPreparedStatement = "SELECT \"Reimbursement\".\"f_getEmployeeAddress\"(?)";
	
	//employee claims
	String strGetEmployeeClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getAllClaims\"(?)";
	
	//MANAGER

	//view employee profile
	String strGetEmployeeProfilePreparedStatement = "SELECT \"Reimbursement\".\"f_getEmployeeProfile\"(?)";
	//view employee
	String strGetEmployeePreparedStatement = "SELECT \"Reimbursement\".\"f_getEmployee\"(?)";
	//view employees
	String strGetAllEmployeePreparedStatement = "SELECT \"Reimbursement\".\"f_getAllEmployees\"()";
	//update employee
	String strChangeEmployeePreparedStatement = "SELECT \"Reimbursement\".\"f_changeEmployee\"(?,?,?,?,?,?,?,?,?,?,?,?)";	
	
	//add employee
	String strAddEmployeePreparedStatement = "SELECT \"Reimbursement\".\"f_addEmployee\"(?,?,?,?,?,?,?,?,?,?,?)";
	//remove employee
	String strRemoveEmployeePreparedStatement = "SELECT \"Reimbursement\".\"f_removeEmployee\"(?)";
	
	//username exists
	String strUsernameExistsPreparedStatement = "SELECT \"Reimbursement\".\"f_userExists\"(?)";
	
	//unlock account
	String strUnlockAccountPreparedStatement = "SELECT \"Reimbursement\".\"f_unlockAccount\"(?)";
	
	//get all employees
	String strGetAllEmployeesPreparedStatement = "SELECT \"Reimbursement\".\"f_getAllEmployees\"()";

	//all reimbursements
	String strGetAllReimbursementsPreparedStatement0 = "SELECT \"Reimbursement\".\"f_getAllReimbursements\"()";

	//approve reimbursement
	String strApproveClaimPreparedStatement = "SELECT \"Reimbursement\".\"f_approveClaim\"(?,?)";
	//reject reimbursement
	String strRejectClaimPreparedStatement = "SELECT \"Reimbursement\".\"f_rejectClaim\"(?,?)";


	//filter reimbursements
	//employee name

	//all reimbursements
	String strGetAllClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getAllClaims\"()";
	String strGetManagerApprovedClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getManagerApprovedClaims\"(?)";
	String strGetPendingClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getPendingClaims\"()";
	String strGetRejectedClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getRejectedClaims\"()";

	//employee reimbursements
	String strEmployeeClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getAllClaims\"(?)";
	String strGetEmployeeApprovedClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getApprovedClaims\"(?)";
	String strGetEmployeePendingClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getPendingClaims\"(?)";
	String strGetEmployeeRejectedClaimsPreparedStatement = "SELECT \"Reimbursement\".\"f_getRejectedClaims\"(?)";
	
	//employee options
	String strGetEmployeeFullNameIDPreparedStatement = "SELECT \"Reimbursement\".\"f_getEmployeeOptions\"()";
	
	//verify employee
	String strValidateEmployeeRegistrationPreparedStatement = "SELECT \"Reimbursement\".\"f_EmployeeExists\"(?)";


	
	
	public DatabaseCommandRepository() {
		try {
	    	Class.forName("org.postgresql.Driver");
	    	DriverManager.registerDriver(new org.postgresql.Driver());
			//logger.trace("The Command Repository Constructor Just Ran and the Driver was Registered, an Unexpected Necessary Step!");
		}catch(ClassNotFoundException cnfe) {
		}catch(SQLException se) {
		}
	}


	public void makePreparedStatement(String _strpreparedstatement) throws SQLException {

		//logger.trace("The Command Repository Constructor Just Ran!");
		
		String url = "jdbc:postgresql://java-fs210907-rds.cqk9npwqkd0p.us-east-1.rds.amazonaws.com:5432/postgres?user=postgres&password=team1pwd";
		connection = DriverManager.getConnection(url);
		preparedstatement = connection.prepareStatement(_strpreparedstatement);

	}


	//TRAFFIC

	//log traffic
	public Object addTraffic(String _strsession, String _straddress) throws SQLException {		
		
		//logger.trace("Add Traffic!");
		
		makePreparedStatement(strAddTrafficPreparedStatement);
		preparedstatement.setInt(1,Integer.valueOf(_strsession));
		preparedstatement.setString(2,_straddress);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		Integer integer = rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		return -1;
		
	}


	public Object getEmployeeOptions() throws SQLException {
		
		//logger.trace("Employee Options!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		makePreparedStatement(strGetEmployeeFullNameIDPreparedStatement);

		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			String strid1 = strresults[0];//id
			String strid3 = strresults[1].replace("\"","");//Last Name
			String strid2 = strresults[2].replace("\"","");//First Name
			
			arrlstobjects.add(strid1);
			arrlstobjects.add(strid2);
			arrlstobjects.add(strid3);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();
		
		return arrlstarrlstobjects;

	}
	
	
	
	//USER	
	
	public Object getAllClaims(String _struserid) throws SQLException, ArrayIndexOutOfBoundsException {
		
		
		//logger.trace("All the Claims!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetEmployeeClaimsPreparedStatement);
		
		preparedstatement.setInt(1,Integer.valueOf(_struserid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id
			int intid2 = Integer.valueOf(strresults[1]);//type
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			Integer intreason = Integer.valueOf(strresults[6]);//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(intreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();
		

		return arrlstarrlstobjects;		

	}


	//get employee
	//remove the employee
	public Object getEmployee(String _struserid) throws SQLException {
		
		//logger.trace("Information for an Employee! ... " + _struserid + "...that guy!!!");
		
		Object o = null;
		
		List<Object> arrlstobjects = new ArrayList<Object>();		
		
		makePreparedStatement(strGetEmployeePreparedStatement);
		preparedstatement.setInt(1,Integer.valueOf(_struserid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {
			
			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");
			
			String strfirstname = strresults[1].replace("\"","").trim();
			String strlastname = strresults[2].replace("\"","").trim();
			String strworkphonenumber = strresults[3].replace("\"","").trim();
			strworkphonenumber = strworkphonenumber.substring(0,3) + "-" + strworkphonenumber.substring(3,6) + "-" + strworkphonenumber.substring(6,10);
			String strhomephonenumber = strresults[4].replace("\"","").trim();
			strhomephonenumber = strhomephonenumber.substring(0,3) + "-" + strhomephonenumber.substring(3,6) + "-" + strhomephonenumber.substring(6,10);
			String strstreet1 = strresults[5].replace("\"","").trim();
			String strstreet2 = strresults[6].replace("\"","").trim();
			String strstreet3 = strresults[7].replace("\"","").trim();
			String strcity = strresults[8].replace("\"","").trim();
			String strstate = strresults[9].replace("\"","").trim();
			String strzipcode = strresults[10].replace("\"","").trim();
			
			arrlstobjects.add(strfirstname);
			arrlstobjects.add(strlastname);
			arrlstobjects.add(strworkphonenumber);
			arrlstobjects.add(strhomephonenumber);
			arrlstobjects.add(strstreet1);
			arrlstobjects.add(strstreet2);
			arrlstobjects.add(strstreet3);
			arrlstobjects.add(strcity);
			arrlstobjects.add(strstate);
			arrlstobjects.add(strzipcode);

		}
		
		o = arrlstobjects;
		
		return o;
		
	}

	
	//user locked out
	public Object userLockedOut(String _strusername, String _stripaddress) throws SQLException {
		
		//logger.trace("The user lock test function.");
		
		Object o = null;
		
		//log the user on
		makePreparedStatement(strUserLockedPreparedStatement);
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_stripaddress);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		Integer integer = rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		return integer;
		
	}


	//user locked out
	public Object managerLockedOut(String _strusername, String _stripaddress) throws SQLException {
		
		//logger.trace("The manager lock test function.");
		
		Object o = null;
		
		//log the user on
		makePreparedStatement(strManagerLockedPreparedStatement);
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_stripaddress);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		Integer integer = rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		return integer;
		
	}


	//logon user
	public Object logonUser(String _strusername, String _strpassword, String _stripaddress) throws SQLException {
		
		//logger.trace("Log a user on!");
		
		//log the user on
		makePreparedStatement(strUserLogonPreparedStatement);
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_strpassword);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		Integer integer = rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		
		//add the session
		makePreparedStatement(strAddSessionPreparedStatement);
		preparedstatement.setInt(1,integer);
		preparedstatement.setString(2,_stripaddress);

		preparedstatement.executeQuery();

		rs.close();
		preparedstatement.close();


		return integer;		
		
	}


	//logoff user
	public Object logoffUser(String _strremoteurl) throws SQLException {		

		//logger.trace("Log off a user!");
		
		Object o = null;
		makePreparedStatement(strUserLogoffPreparedStatement);
		preparedstatement.setString(1,_strremoteurl);

		ResultSet rs = preparedstatement.executeQuery();

		rs.close();
		preparedstatement.close();		

		return o;
		
	}

	//logon manager
	public Object logonManager(String _strusername, String _strpassword, String _stripaddress) throws SQLException {
		
		//logger.trace("Log on a manager!");
		
		//log the user on
		makePreparedStatement(strManagerLogonPreparedStatement);
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_strpassword);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		Integer integer = rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		
		//add the session
		makePreparedStatement(strAddSessionPreparedStatement);
		preparedstatement.setInt(1,integer);
		preparedstatement.setString(2,_stripaddress);

		preparedstatement.executeQuery();

		rs.close();
		preparedstatement.close();


		return integer;		
		
	}


	//VIEW ALL CLAIMS
	
	//get all claims
	public Object getAllClaims() throws SQLException {
		
		//logger.trace("Get all claims for everyone!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetAllClaimsPreparedStatement);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id
			int intid2 = -1; 
			try {
				intid2 = Integer.valueOf(strresults[1]);	
			}catch(NumberFormatException nfe) {}
			
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			String strreason = strresults[6];//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(strreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();
		
		
		return arrlstarrlstobjects;		
		
	}
	
	//get all approved claims
	public Object getAllApprovedClaims(String _strid) throws SQLException {
		
		//logger.trace("Get all approved claims for everyone!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetManagerApprovedClaimsPreparedStatement);
		
		preparedstatement.setInt(1,Integer.valueOf(_strid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id

			//type
			int intid2 = -1; 
			try {
				intid2 = Integer.valueOf(strresults[2]);	
			}catch(NumberFormatException nfe) {}
			
			int intid3 = Integer.valueOf(strresults[3]);//project id
			String str = strresults[4];//comments
			String strdt = strresults[5];//date
			String strexpensedt = strresults[6];//expense date
			Integer intreason = Integer.valueOf(strresults[7]);//reason
			Float fltamt = Float.valueOf(strresults[8]);//amount
			String strmanager = strresults[9];
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(intreason);
			arrlstobjects.add(fltamt);
			arrlstobjects.add(strmanager);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}

	
	//get all rejected claims
	public Object getAllRejectedClaims() throws SQLException {
		
		//logger.trace("Get all rejected claims for everyone!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetRejectedClaimsPreparedStatement);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id

			//type
			int intid2 = -1; 
			try {
				intid2 = Integer.valueOf(strresults[1]);	
			}catch(NumberFormatException nfe) {}
			
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			Integer intreason = Integer.valueOf(strresults[6]);//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(intreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}
	
	//get all pending claims
	public Object getAllPendingClaims() throws SQLException {
		
		//logger.trace("Get all pending claims for everyone!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetPendingClaimsPreparedStatement);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id
			
			//type
			int intid2 = -1; 
			try {
				intid2 = Integer.valueOf(strresults[1]);	
			}catch(NumberFormatException nfe) {}
			
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			String strreason = strresults[6];//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(strreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}	

	//ADD CLAIM
	
	public Object addClaim(String _strprojectid, String _strtype, String _strreason, String _strexpensedate, String _stramount) throws SQLException {

		//logger.trace("Add a claim!");
		
		Object o = null;

		makePreparedStatement(strAddClaimPreparedStatement);
		
		System.out.println("------" + _strreason + "-------------");

		preparedstatement.setString(1,_strtype);
		preparedstatement.setString(2,_strreason);
		preparedstatement.setInt(3, Integer.valueOf(_strprojectid));
		preparedstatement.setString(4,_strexpensedate);
		preparedstatement.setFloat(5,Float.valueOf(_stramount));

		ResultSet rs = preparedstatement.executeQuery();

		return o;

	}
	
	
	
	
	
	
	
	
	
	
	//VIEW EMPLOYEE CLAIMS
	
	//get all approved claims
	public Object getEmployeeApprovedClaims(String _strlogonid) throws SQLException, ArrayIndexOutOfBoundsException {
		
		//logger.trace("Get Approved Claims for an Employee!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetEmployeeApprovedClaimsPreparedStatement);
		//set the logon id parameter
		preparedstatement.setInt(1,Integer.valueOf(_strlogonid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id
			int intid2 = Integer.valueOf(strresults[1]);//type
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			String strreason = strresults[6];//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(strreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}
	
	//get all rejected claims
	public Object getEmployeeRejectedClaims(String _strlogonid) throws SQLException, ArrayIndexOutOfBoundsException {
		
		//logger.trace("Get Rejected Claims for an Employee!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetEmployeeRejectedClaimsPreparedStatement);
		//set the logon id parameter
		preparedstatement.setInt(1,Integer.valueOf(_strlogonid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id
			int intid2 = Integer.valueOf(strresults[1]);//type
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			String strreason = strresults[6];//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(strreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}
	
	
	public Object getUserID(String _strusername, String _strpassword) throws SQLException {
	
		//logger.trace("Get the User ID for a Logon");
		
		Object o = null;
	
		//log the manager on
		makePreparedStatement(strGetUserIDPreparedStatement);
		//set the logon id parameter
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_strpassword);
		
		ResultSet rs = preparedstatement.executeQuery();
	
		rs.next();
		o = rs.getInt(1);	
	
		System.out.println("---------------" + _strusername + " : " + _strpassword + ":::::" + o.toString() + ":::::------------------");
		
		return o;
	
	}
	
	
	
	//get all pending claims
	public Object getEmployeePendingClaims(String _strlogonid) throws SQLException, ArrayIndexOutOfBoundsException {
		
		//logger.trace("Get Pending Claims for an Employee!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetEmployeePendingClaimsPreparedStatement);
		//set the logon id parameter
		preparedstatement.setInt(1,Integer.valueOf(_strlogonid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");//id
			
			int intid1 = Integer.valueOf(strresults[0]);//id
			int intid2 = Integer.valueOf(strresults[1]);//type
			int intid3 = Integer.valueOf(strresults[2]);//project id
			String str = strresults[3];//comments
			String strdt = strresults[4];//date
			String strexpensedt = strresults[5];//expense date
			String strreason = strresults[6];//reason
			Float fltamt = Float.valueOf(strresults[7]);//amount
			
			//System.out.println(intid1 + ", " + intid2 + ", " + intid3 + ", " + str + ", " + strdt.toString() + ", " + strexpensedt.toString() + ", " + intreason + ", " + fltamt);

			arrlstobjects.add(intid1);
			arrlstobjects.add(intid2);
			arrlstobjects.add(intid3);
			arrlstobjects.add(str);
			arrlstobjects.add(strdt);
			arrlstobjects.add(strexpensedt);
			arrlstobjects.add(strreason);
			arrlstobjects.add(fltamt);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}	
	

	//remove employee
	public Object removeEmployee(Integer _userid) throws SQLException {	
		
		//logger.trace("Remove an Employee!");
		
		makePreparedStatement(strRemoveEmployeePreparedStatement);
		preparedstatement.setInt(1,_userid);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.close();
		preparedstatement.close();
		
		return -1;
		
	}
	
	//lock employee
	public Object lockOutUser(String _strusername, String _strremoteurl) throws SQLException {	
		
		//logger.trace("Lock out a User!");
		
		makePreparedStatement(strLockOutUserPreparedStatement);
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_strremoteurl);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.close();
		preparedstatement.close();
		
		return -1;
		
	}

	
	//MANAGER
	
	//get all employees
	public Object getAllEmployees() throws SQLException{
		
		//logger.trace("Get the Employees!");
		
		List<List<Object>> arrlstarrlstobjects = new ArrayList<List<Object>>();
		
		//log the manager on
		makePreparedStatement(strGetAllEmployeesPreparedStatement);
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			List<Object> arrlstobjects = new ArrayList<Object>();

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");
			
			String struserid = strresults[0].replace("\"","").trim();
			String strfirstname = strresults[1].replace("\"","").trim();
			String strlastname = strresults[2].replace("\"","").trim();
			String strworkphonenumber = strresults[3].replace("\"","").trim();
			strworkphonenumber = strworkphonenumber.substring(0,3) + "-" + strworkphonenumber.substring(3,6) + "-" + strworkphonenumber.substring(6,10);
			String strhomephonenumber = strresults[4].replace("\"","").trim();
			strhomephonenumber = strhomephonenumber.substring(0,3) + "-" + strhomephonenumber.substring(3,6) + "-" + strhomephonenumber.substring(6,10);
			String strstreet1 = strresults[5].replace("\"","").trim();
			String strstreet2 = strresults[6].replace("\"","").trim();
			String strstreet3 = strresults[7].replace("\"","").trim();
			String strcity = strresults[8].replace("\"","").trim();
			String strstate = strresults[9].replace("\"","").trim();
			String strzipcode = strresults[10].replace("\"","").trim();
			
			arrlstobjects.add(struserid);
			arrlstobjects.add(strfirstname);
			arrlstobjects.add(strlastname);
			arrlstobjects.add(strworkphonenumber);
			arrlstobjects.add(strhomephonenumber);
			arrlstobjects.add(strstreet1);
			arrlstobjects.add(strstreet2);
			arrlstobjects.add(strstreet3);
			arrlstobjects.add(strcity);
			arrlstobjects.add(strstate);
			arrlstobjects.add(strzipcode);
			
			arrlstarrlstobjects.add(arrlstobjects);
			
		}
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstarrlstobjects;		
		
	}
	
	public List<Object> getEmployeeAddress(String _stremployeeid) throws SQLException{
		
		//logger.trace("Get an Employee's Address!");
		
		List<Object> arrlstobj = new ArrayList<Object>();
		
		System.out.println("Method: " + _stremployeeid);
		
		//log the manager on
		makePreparedStatement(strGetEmployeeAddressPreparedStatement);
		preparedstatement.setInt(1,Integer.valueOf(_stremployeeid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();

		String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");
		
		String straddress1 = strresults[1].replace("\"","").trim();
		String straddress2 = strresults[2].replace("\"","").trim();
		String straddress3 = strresults[3].replace("\"","").trim();
		String strcity = strresults[4].replace("\"","").trim();
		String strstate = strresults[5].replace("\"","").trim();
		Integer intzipcode5 = Integer.valueOf(strresults[6]);
		Integer intzipcode4 = Integer.valueOf(strresults[7]);

		arrlstobj.add(straddress1);
		arrlstobj.add(straddress2);
		arrlstobj.add(straddress3);
		arrlstobj.add(strcity);
		arrlstobj.add(strstate);
		arrlstobj.add(intzipcode5);
		arrlstobj.add(intzipcode4);
		
		rs.close();
		preparedstatement.close();		
		
		return arrlstobj;
		
	}
	

	//add employee
	public Object addEmployee(String _strusername, String _strtemporarypassword, String _strfirstname, String _strlastname, String _strworkphonenumber, String _strhomephonenumber, String _strstreet1, String _strstreet2, String _strcity, String _strstate, String _strzipcode) throws SQLException{

		//logger.trace("Add an Employee!");
		
		Object o = null;
		
		makePreparedStatement(strAddEmployeePreparedStatement);
		
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_strtemporarypassword);
		preparedstatement.setString(3,_strfirstname);
		preparedstatement.setString(4,_strlastname);
		preparedstatement.setString(5,_strworkphonenumber);
		preparedstatement.setString(6,_strhomephonenumber);
		preparedstatement.setString(7,_strstreet1);
		preparedstatement.setString(8,_strstreet2);
		preparedstatement.setString(9,_strcity);
		preparedstatement.setString(10,_strstate);
		preparedstatement.setString(11,_strzipcode.trim());		
		
		ResultSet rs = preparedstatement.executeQuery();
		rs.next();

		o = (Integer)rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		return o;
		
	}


	//get employee profile
	public Object getEmployeeProfile(String _struserid) throws SQLException {
		
		Object o = null;
		
		List<Object> arrlstobjects = new ArrayList<Object>();	
		
		makePreparedStatement(strGetEmployeeProfilePreparedStatement);
		preparedstatement.setInt(1,Integer.valueOf(_struserid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		while(rs.next()) {

			String [] strresults = rs.getString(1).replace("(","").replace(")","").split(",");
			
			String strid = strresults[0].replace("\"","").trim();
			String strfirstname = strresults[1].replace("\"","").trim();
			String strlastname = strresults[2].replace("\"","").trim();
			String strworkphonenumber = strresults[3].replace("\"","").trim();
			strworkphonenumber = strworkphonenumber.substring(0,3) + "-" + strworkphonenumber.substring(3,6) + "-" + strworkphonenumber.substring(6,10);
			String strhomephonenumber = strresults[4].replace("\"","").trim();
			strhomephonenumber = strhomephonenumber.substring(0,3) + "-" + strhomephonenumber.substring(3,6) + "-" + strhomephonenumber.substring(6,10);
			String strstreet1 = strresults[5].replace("\"","").trim();
			String strstreet2 = strresults[6].replace("\"","").trim();
			String strstreet3 = strresults[7].replace("\"","").trim();
			String strcity = strresults[8].replace("\"","").trim();
			String strstate = strresults[9].replace("\"","").trim();
			String strzipcode = strresults[10].replace("\"","").trim();
			String strusername = strresults[11].replace("\"","").trim();
			String strpassword = strresults[12].replace("\"","").trim();
			
			arrlstobjects.add(strid);
			arrlstobjects.add(strfirstname);
			arrlstobjects.add(strlastname);
			arrlstobjects.add(strworkphonenumber);
			arrlstobjects.add(strhomephonenumber);
			arrlstobjects.add(strstreet1);
			arrlstobjects.add(strstreet2);
			arrlstobjects.add(strstreet3);
			arrlstobjects.add(strcity);
			arrlstobjects.add(strstate);
			arrlstobjects.add(strzipcode);
			arrlstobjects.add(strusername);
			arrlstobjects.add(strpassword);

		}
		
		o = arrlstobjects;
		
		return o;
		
	}
	
	
	//update employee
	public Object updateEmployee(String _struserid, String _strusername, String _strpassword, String _strfirstname, String _strlastname, String _strworkphonenumber, String _strhomephonenumber, String _strstreet1, String _strstreet2, String _strcity, String _strstate, String _strzipcode) throws SQLException {

		//logger.trace("Update an Employee's Logon and/or Information!");
		
		Object o = null;
		
		makePreparedStatement(strChangeEmployeePreparedStatement);
		
		preparedstatement.setInt(1,Integer.valueOf(_struserid));
		preparedstatement.setString(2,_strusername);
		preparedstatement.setString(3,_strpassword);
		preparedstatement.setString(4,_strfirstname);
		preparedstatement.setString(5,_strlastname);
		preparedstatement.setString(6,_strworkphonenumber.replace("-", ""));
		preparedstatement.setString(7,_strhomephonenumber.replace("-", ""));
		preparedstatement.setString(8,_strstreet1);
		preparedstatement.setString(9,_strstreet2);
		preparedstatement.setString(10,_strcity);
		preparedstatement.setString(11,_strstate.trim());
		preparedstatement.setString(12,_strzipcode.trim());
		
		ResultSet rs = preparedstatement.executeQuery();
		rs.next();

		o = (Integer)rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		return o;
		
	}	
	

	public Object getManagerID(String _strusername, String _strpassword) throws SQLException {
		
		//logger.trace("Get a Manager's ID!");
		
		
		Object o = null;
		
		//log the manager on
		makePreparedStatement(strGetManagerIDPreparedStatement);
		//set the logon id parameter
		preparedstatement.setString(1,_strusername);
		preparedstatement.setString(2,_strpassword);
		
		ResultSet rs = preparedstatement.executeQuery();
	
		rs.next();
		o = rs.getInt(1);	
	
		System.out.println("---------------" + _strusername + " : " + _strpassword + ":::::" + o.toString() + ":::::------------------");
		
		return o;

	}
	
	//approve claim
	public Object approveClaim(String _strid, String _strmanagerid) throws SQLException {
		
		//logger.trace("Approve a Claim!");
		
		Object o = null;
		
		//log the user on
		makePreparedStatement(strApproveClaimPreparedStatement);
		preparedstatement.setInt(1,Integer.valueOf(_strid));
		preparedstatement.setInt(2,Integer.valueOf(_strmanagerid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		
		rs.close();
		preparedstatement.close();
		
		return null;
		
	}
	
	//reject claim
	public Object rejectClaim(String _strid, String _strmanagerid) throws SQLException {
		
		//logger.trace("Reject a Claim!");
		
		Object o = null;
		
		//log the user on
		makePreparedStatement(strRejectClaimPreparedStatement);
		preparedstatement.setInt(1,Integer.valueOf(_strid));
		preparedstatement.setInt(2,Integer.valueOf(_strmanagerid));
		
		ResultSet rs = preparedstatement.executeQuery();
		
		rs.next();
		Integer integer = rs.getInt(1);
		
		rs.close();
		preparedstatement.close();
		
		return null;
		
	}
	
}

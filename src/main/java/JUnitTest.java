import java.sql.SQLException;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class JUnitTest {
	
   public static void main(String[] args) throws SQLException {
	   
	  new CurrentUserTest().testCurrentUserTest();
	  new EmployeeViewServletTest().testEmployeeViewServletTest();	  
	  new ChangeReimbursementsServletTest().testChangeReimbursementsServletTest();
	  new LoginServletTest().testLoginServletTest();
	  new LogoffServletTest().testLogoffServletTest();
	  new ViewReimbursementsServletTest().testViewReimbursementsServletTest();
	  
	  new DatabaseServiceTest().testDatabaseServiceTest();
	  
	  //Menus
	  new ParameterHandlerTest().testParameterHandlerTest();
	  new ProcessFormTest().testProcessFormTest();

	  new ParameterHandlerTest().testParameterHandlerTest();
	  new ProcessFormTest().testProcessFormTest();
	  
	  new TableFactoryTest().testTableFactoryTest();
	  new WebPageFactoryTest().testWebPageFactoryTest();
	  
 
   }
} 
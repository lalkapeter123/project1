import java.util.ArrayList;
import java.util.List;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import junit.framework.Assert;

public class CurrentUserTest {
	
	public void testCurrentUserTest() {
		
		Assert.assertEquals(-1, Data.CurrentUser.intsessionid);
		Assert.assertEquals(-1, Data.CurrentUser.intuserid);
		Assert.assertEquals("", Data.CurrentUser.strfirstname);
		Assert.assertEquals("", Data.CurrentUser.strlastname);
		Assert.assertEquals("", Data.CurrentUser.strcity);
		Assert.assertEquals("", Data.CurrentUser.strhomephonenumber);
		Assert.assertEquals("", Data.CurrentUser.strworkphonenumber);
		Assert.assertEquals("", Data.CurrentUser.strremoteipaddress);
		Assert.assertEquals("", Data.CurrentUser.strstate);
		Assert.assertEquals("", Data.CurrentUser.strstreet1);
		Assert.assertEquals("", Data.CurrentUser.strstreet2);
		Assert.assertEquals("", Data.CurrentUser.strzipcode);

		
		List<Object> arrlstobject = new ArrayList<Object>();		
		arrlstobject.add("firstname");
		arrlstobject.add("lastname");
		arrlstobject.add("1234567890");
		arrlstobject.add("0987654321");
		arrlstobject.add("s1");
		arrlstobject.add("s2");
		arrlstobject.add("");
		arrlstobject.add("city");
		arrlstobject.add("state");
		arrlstobject.add("11111");
		
		//set the user information variables
		Data.CurrentUser.setUserInformationVariables(arrlstobject);
		
		Assert.assertEquals("firstname", Data.CurrentUser.strfirstname);
		Assert.assertEquals("lastname", Data.CurrentUser.strlastname);
		Assert.assertEquals("city", Data.CurrentUser.strcity);
		Assert.assertEquals("0987654321", Data.CurrentUser.strhomephonenumber);
		Assert.assertEquals("1234567890", Data.CurrentUser.strworkphonenumber);
		Assert.assertEquals("", Data.CurrentUser.strremoteipaddress);
		Assert.assertEquals("state", Data.CurrentUser.strstate);
		Assert.assertEquals("s1", Data.CurrentUser.strstreet1);
		Assert.assertEquals("s2", Data.CurrentUser.strstreet2);
		Assert.assertEquals("11111", Data.CurrentUser.strzipcode);
		
		
		
		
		
		ArrayList<String> arrlstobject1 = new ArrayList<String>();
		arrlstobject1.add("");
		arrlstobject1.add("");
		arrlstobject1.add("");
		arrlstobject1.add("firstname");
		arrlstobject1.add("lastname");
		arrlstobject1.add("1234567890");
		arrlstobject1.add("0987654321");
		arrlstobject1.add("s1");
		arrlstobject1.add("s2");
		arrlstobject1.add("city");
		arrlstobject1.add("state");
		arrlstobject1.add("11111");		
		
		Data.CurrentUser.setUserInformationVariablesFromParameters(arrlstobject1);
		Assert.assertEquals("firstname",Data.CurrentUser.strfirstname);
		Assert.assertEquals("lastname", Data.CurrentUser.strlastname);
		Assert.assertEquals("city", Data.CurrentUser.strcity);
		Assert.assertEquals("0987654321", Data.CurrentUser.strhomephonenumber);
		Assert.assertEquals("1234567890", Data.CurrentUser.strworkphonenumber);
		Assert.assertEquals("", Data.CurrentUser.strremoteipaddress);
		Assert.assertEquals("state", Data.CurrentUser.strstate);
		Assert.assertEquals("s1", Data.CurrentUser.strstreet1);
		Assert.assertEquals("s2", Data.CurrentUser.strstreet2);
		Assert.assertEquals("11111", Data.CurrentUser.strzipcode);
		
		Data.CurrentUser.setUserID(9);
		Assert.assertEquals(9,Data.CurrentUser.getUserID());
		
		Data.CurrentUser.setSessionID(9);
		Assert.assertEquals(9,Data.CurrentUser.getSessionID());

		Data.CurrentUser.setRemoteIPAddress("0:0:0:0:0:0:0:1");
		Assert.assertEquals("0:0:0:0:0:0:0:1",Data.CurrentUser.getRemoteIPAddress());
		
	}

}

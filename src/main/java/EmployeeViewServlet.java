//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import Factories.TableFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DatabaseCommands.DatabaseService;
import Factories.WebPageFactory;
import Data.ParameterHandler;

public class EmployeeViewServlet extends HttpServlet {
	
	//private static final Logger logger = LogManager.getLogger(EmployeeViewServlet.class);
	
	//an object that loads parameters from database service calls
	ParameterHandler parameterhandler = new ParameterHandler();
	//an object that returns headers and forms
	WebPageFactory webpagefactory = new WebPageFactory(); 
	List<String> arrlststrs = new ArrayList<String>();

	public void init(){}


	//GET
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		//if there is a valid session id in the current user object
		if(Data.CurrentUser.intsessionid != -1) {
			
			
			//logger.trace("We've just entered the doGet function of the EmployeeViewServlet class!");
			
		    String strrequesturl = request.getRequestURL().toString();
	
			//System.out.println("------------------" + strrequesturl);
			
		    String strresponse = "";
		    
			if(strrequesturl.endsWith("/RemoveEmployee.html")) {
				
				//logger.trace("The remove employee command!  This isn't a possible path!");
				
				Integer id = Integer.valueOf(request.getParameter("id"));
	
				try {
					
					new DatabaseService().getCommand("remove employee",new Object[] {id});
					arrlststrs.remove(id);
					
					String strmanagerremoveemployeefooter = webpagefactory.createManagerWebPageHeader();
					String message = webpagefactory.removedEmployee(String.valueOf(id));
					String strmanageremployeeviewfooter = "</body>\r\n</html>";
					
					printHTML(strmanagerremoveemployeefooter + message + strmanageremployeeviewfooter, response);
					
				} catch (SQLException se) {
					se.printStackTrace();
				}
				
			} else if(strrequesturl.endsWith("/Employee.html")) {
	
				//logger.trace("Back to the employee landing page!");
					
				String stremployeeviewheader = new WebPageFactory().createEmployeeHeader();
				String stremployeeviewtable = new TableFactory().createEmployeeTableView();
				String stremployeeviewfooter = "</body>\r\n</html>";
				
				printHTML(stremployeeviewheader + stremployeeviewtable + stremployeeviewfooter, response);
				
				
			} else if(strrequesturl.endsWith("Employees.html")) {
			      
				//logger.trace("Now a manager wants to view the employees!");
				
				try {
					
					String stremployeeviewheader = webpagefactory.createManagerWebPageHeader();
					String stremployeeviewtable = new TableFactory().createEmployeeTable();
					String stremployeeviewfooter = "</body>\r\n</html>";
					
					printHTML(stremployeeviewheader + stremployeeviewtable + stremployeeviewfooter, response);
					
				} catch (SQLException e) {
					//TODO Auto-generated catch block
					//e.printStackTrace();
				}
			} else if(strrequesturl.endsWith("ManagerAddEmployee.html")) {
				
				//logger.trace("Now a manager wants to add an employee!");
				
				String strmanagerviewheader = webpagefactory.createManagerWebPageHeader();
				String strresponse1 = webpagefactory.createAddEmployeeWebPage();
				String stremployeeviewfooter = "</body>\r\n</html>";
	
				printHTML(strmanagerviewheader + strresponse1 + stremployeeviewfooter, response);
			
			//change employee
			} else if(strrequesturl.endsWith("ManagerChangeEmployee.html")) {
	
				try {
					
					//logger.trace("Now a manager wants to change an employee!");
					
					String strid = request.getParameter("id");
					String strmanagerwebpageheader = webpagefactory.createManagerWebPageHeader();
					String strchangeemployeeform = webpagefactory.createManagerChangeEmployeeForm(strid);
					String stremployeeviewfooter = "</body>\r\n</html>";
					
					printHTML(strmanagerwebpageheader + strchangeemployeeform + stremployeeviewfooter, response);
				
				} catch(SQLException se) {
					se.printStackTrace();
				}
	
			//employee view
			} else if(strrequesturl.endsWith("ManagerEmployeeView.html")) {
				
				//logger.trace("Now a manager wants to view an employee!");
				
				String strid = request.getParameter("id");
				
				try {
	
					String strmanagerwebpageheader = webpagefactory.createManagerWebPageHeader();
					String stremployeeprofileview = webpagefactory.createViewEmployeeProfileWebPage(strid);
					String stremployeeviewfooter = "</body>\r\n</html>";			
						
					printHTML(strmanagerwebpageheader + stremployeeprofileview + stremployeeviewfooter, response);
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		//there is not a valid session id
		} else {
			String strremoteaddress = request.getRemoteAddr();
	        String strwebpage = request.getHeader("referer");
			redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/EmployeeLogin.html", response);
		}
	}


	//POST
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		//logger.trace("We've just entered the doPost function of the EmployeeViewServlet class!");
		
	    String strwebpage = request.getRequestURL().toString();
		
	    //add an employee
	    if(strwebpage.endsWith("ManagerAddEmployee.html")) {	    	

			//logger.trace("Now a manager wants to add an employee!");
			
	    	String [] arrstr = parameterhandler.getEmployeeInformation(request);
	    		
	    	new Data.ProcessForm().processAddEmployee(arrstr);

			String strmanagerviewheader = webpagefactory.createManagerWebPageHeader();
			String stremployeeviewfooter = "</body>\r\n</html>";
	    	String strmessage = "<span style=\"font-family:sans-serif\">" + arrstr[2] + " " + arrstr[3] + " was added as an empoyee.</span>";		    
			
	    	printHTML(strmanagerviewheader + strmessage + stremployeeviewfooter, response);    	

		//change an employee	    	
	    } else if(strwebpage.endsWith("ManagerChangeEmployee.html")) {
	    	
			//logger.trace("Now a manager wants to change an employee!");
			
	    	String [] arrstr = parameterhandler.getEmployeeProfile(request);

	    	//process the form and change the employee information
	    	new Data.ProcessForm().processChangeEmployee(arrstr);

	    	//send the manager web page header and a channged employee message
	    	String strresponse = webpagefactory.createManagerWebPageHeader();
	    	String strresponsemanagerheader = "<span style=\"font-family:sans-serif\">" + arrstr[3] + " " + arrstr[4] + " was changed.</span>\r\n</body>\r\n			</html>";
	    	
			printHTML(strresponse + strresponsemanagerheader, response);    	
	    	
	    }	    	
	}


	public void destroy(){}
	
	public void printHTML(String _strhtml, HttpServletResponse _response) throws IOException {		
		_response.setContentType("text/html");
		PrintWriter printwriter = _response.getWriter();
		printwriter.print(_strhtml);
		printwriter.close();		
	}

   //redirect to the user to a webpage
   public void redirectTo(String _strremoteaddress, String _strwebpage, String _strredirectto, HttpServletResponse _response) throws IOException {
	   _response.setHeader(_strremoteaddress, _strwebpage);
	   _response.setContentType("text/html");
	   _response.sendRedirect(_strredirectto);
   }

}

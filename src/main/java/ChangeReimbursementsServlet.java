//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Data.ParameterHandler;
import DatabaseCommands.DatabaseService;
import Factories.TableFactory;
import Factories.WebPageFactory;

public class ChangeReimbursementsServlet extends HttpServlet {

	//private static final Logger logger = LogManager.getLogger(ChangeReimbursementsServlet.class);
	
	WebPageFactory webpagefactory = new WebPageFactory(); 
	TableFactory tablefactory = new TableFactory();
	ParameterHandler parameterbuilder = new ParameterHandler(); 
	
	public void init() {}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		
		//if there is a valid session id in the current user object
		if(Data.CurrentUser.intsessionid != -1) {
			
	
			//logger.trace("We've just started the doGet function!");
			
			String strrequesturl = request.getRequestURL().toString();
			String strwebpage = request.getHeader("referer").toString();
			
			if(strrequesturl.endsWith("EmployeeAddReimbursement.html")) {
	
				//logger.trace("Now the user wants to add a claim!");
				
				String strheader = webpagefactory.createEmployeeHeader();
				String strform = "<form action=\"./AddReimbursement\" target=\"\" method=\"POST\">\r\n  "
						+ "<table style=\"font-family:sans-serif;font-size:9pt\">\r\n"
						+ "    <tr>\r\n"
						+ "      <td width=100>\r\n"
						+ "      <!-- include a menu on the left -->\r\n      </td>\r\n"
						+ "      <td>\r\n        "
						+ "      <table>\r\n"
						+ "          <tr>\r\n"
						+ "             <td style=\"font-family:11pt;font-size:15pt;height:15pt\" class=\"login_form\" align=center colspan=2>Add Reimbursement<br /><br /></td>\r\n"
						+ "          </tr>\r\n"
						+ "          <tr>\r\n"
						+ "            <td class=\"tdr\" style=\"height:25pt\">Project:</td>\r\n"
						+ "            <td><input name= \"project\" type=\"text\" style=\"border-color:Orange\" /></td>\r\n"
						+ "          </tr>\r\n"
						+ "            <td class=\"tdr\" style=\"height:25pt\">Type:</td>\r\n"
						+ "            <td>\r\n"
						+ "              <select name=\"type\" style=\"font-family:sans-serif;border-color:Orange\">\r\n"
						+ "                <option value=\"air-travel\">Airfare</option>\r\n"
						+ "                <option value=\"road-travel\">Car Travel</option>\r\n"
						+ "                <option value=\"relocation\">Relocation</option>\r\n"
						+ "              </select>\r\n"
						+ "            </td>\r\n"
						+ "          </tr>\r\n"
						+ "          <tr>\r\n"
						+ "          <td class=\"tdr\" style=\"height:25pt\">Reason:</td>\r\n"
						+ "            <td>\r\n"
						+ "              <select name=\"reason\" style=\"font-family:sans-serif;border-color:Orange\">\r\n"
						+ "                <option value=\"air-travel-to-project\">Air Travel to Project</option>\r\n"
						+ "                <option value=\"road-travel-to-project\">Car Travel to Project</option>\r\n"
						+ "                <option value=\"moving-expense\">Moving Expense</option>\r\n"
						+ "                <option value=\"other-expense\">Other Expense</option>\r\n"
						+ "              </select>\r\n"
						+ "            </td>\r\n"
						+ "          </tr>\r\n"
						+ "          <tr>\r\n"
						+ "            <td class=\"tdr\" style=\"height:25pt\">Amount:</td>\r\n"
						+ "            <td><input name=\"amount\" type=\"text\" style=\"border-color:Orange\" /></td>\r\n"
						+ "          </tr>\r\n"
						+ "          <tr>\r\n"
						+ "            <td class=\"tdr\" style=\"height:25pt\">Expense Date:</td>\r\n"
						+ "            <td><input type=\"date\" name=\"date\" style=\"font-family:sans-serif;color:Gray;border-color:Orange\" /></td>\r\n"
						+ "          </tr>\r\n"
						+ "		  <tr>\r\n"
						+ "            <td class=\"tdr\" style=\"height:25pt\"></td>\r\n"
						+ "            <td><input type=\"submit\" value=\"Add Claim\" style=\"background-color:white;border-color:Orange\" /></td>\r\n"
						+ "		  </tr>\r\n"
						+ "        </table>\r\n"
						+ "      </td>\r\n"
						+ "    </tr>\r\n"
						+ "  </table>\r\n"
						+ "\r\n\r\n</form>";
	
				String strfooter = "</body></html>";
				
				printHTML(strheader + strform + strfooter, response);		
				
			} else if(strrequesturl.endsWith("ManagerChangePendingClaim.html")) {
				
				//logger.trace("The manager approved or rejected a claim!");
				
				try {
	
					//get the parameters
					String[] arrstr = parameterbuilder.getManagerClaimChangeParameters(request);
					
					//action
					String straction = arrstr[0];
					//claim id
					String strid = arrstr[1];
					
					if(straction != null && strid != null) {
						//approve or reject
						if(straction.compareTo("Approve") == 0) {
							//claim id and manager id
							new DatabaseService().getCommand("approve claim",new String[] {strid,String.valueOf(Data.CurrentUser.intuserid)});	
						} else {
							//claim id and manager id
							new DatabaseService().getCommand("reject claim",new String[] {strid,String.valueOf(Data.CurrentUser.intuserid)});
						}
					}
	
					//logger.trace("List the pending claims table for the manager again!");
					String strmanagerchangeclaimheader = new WebPageFactory().createManagerWebPageHeader();
					String strmanagerchangependingtable = "<br /><br /><span style=\"font-family:sans-serif\">Approve/Reject a Claim</span><br /><br />";
					strmanagerchangependingtable += tablefactory.createManagerChangePendingClaimsTable("view pending claims");
					String strmanagerchangeclaimfooter = "\r\n</body>\r\n</html>";
					
					//print html
					printHTML(strmanagerchangeclaimheader + strmanagerchangependingtable + strmanagerchangeclaimfooter, response);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		//there is not a valid session id
		} else {
			String strremoteaddress = request.getRemoteAddr();
	        String strwebpage = request.getHeader("referer");
			redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/EmployeeLogin.html", response);
		}
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		//logger.trace("We've just started the doPost function!");

	    String strrequesturl = request.getRequestURL().toString();

		//Use a relative path for the form action.
		
		try {
			
			if(strrequesturl.endsWith("EmployeeAddReimbursement.html")) {
				
				//logger.trace("Now the employee just added a reimbursement!");
				
				String [] arrstr = parameterbuilder.getClaimChangeParameters(request);
				
				//add claim
				List<List<Object>> arrlstarrlstrecords = (List<List<Object>>)new DatabaseService().getCommand("add claim",arrstr);

				//header
				String strheader = webpagefactory.createEmployeeHeader();
				//message
				String strmessage = "<br /><br /><br /><span style=\"font-family:sans-serif\">Your claim was successfuly added.</span><br /><br /><br />";
				//footer
				String strfooter = "</body></html>";
				
				printHTML(strheader + strmessage + strfooter,response);

			//add claim
			} else if(strrequesturl.endsWith("/Claim/AddReimbursement")) {

				//logger.trace("Now the employee just added a reimbursement!");
				
				String [] arrstr = parameterbuilder.getClaimChangeParameters(request);
				
				//Employee Claims
				List<List<Object>> arrlstarrlstrecords = (List<List<Object>>)new DatabaseService().getCommand("add claim",arrstr);
				
				String strheader = webpagefactory.createEmployeeHeader();
				
				String strmessage = "<br /><br /><br /><span style=\"font-family:sans-serif\">Your claim was successfuly added.</span><br /><br /><br />";				
				String strfooter = "</body></html>";
				
				printHTML(strheader + strmessage + strfooter,response);
				
			} else if(strrequesturl.endsWith("ManagerChangePendingClaim.html")) {

				//logger.trace("Now the manager wants to list the pending claim for approval or rejection!");
				
				//header
				String strmanagerheader = new WebPageFactory().createManagerWebPageHeader();
				//footer
				String strmanagerfooter = "\r\n</body>\r\n</html>";
				//pending
				String strpendingtable = "<br /><br />";

				//Employee Pending Claims
				List<List<Object>> arrlstarrlstrecords = null;
				try {
					arrlstarrlstrecords = (List<List<Object>>)new DatabaseService().getCommand("view employee pending claims",new String[] {"0"});
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				strpendingtable += "<br /><br /><br />";
				strpendingtable += "<table>";
				
				for(List<Object> arrlstobj : arrlstarrlstrecords) {
					strpendingtable += "<tr>";
					//type, projectid, comments, date, expense date, reason, amount
					strpendingtable += "<td>" + arrlstobj.get(1) + "</td>";
					strpendingtable += "<td align=right>" + arrlstobj.get(2) + "</td>";
					strpendingtable += "<td>" + arrlstobj.get(3).toString().replace("\"","") + "</td>";
					strpendingtable += "<td align=center>" + arrlstobj.get(4) + "</td>";
					strpendingtable += "<td align=center>" + arrlstobj.get(5) + "</td>";
					strpendingtable += "<td align=right>" + arrlstobj.get(6) + "</td>";
					strpendingtable += "<td align=right>" + arrlstobj.get(7) + "</td>";
					strpendingtable += "</tr>";
				}

				strpendingtable += "</table>";
				
				printHTML(strmanagerheader + strpendingtable + strmanagerfooter,response);
	
			}
		 
		 
		} catch(IOException e) {
			e.printStackTrace();
		} catch(SQLException se) {
			se.printStackTrace();
		}
			
	}	
	
	public void destroy() {}
	
	public void printHTML(String _strcontent, HttpServletResponse _response) throws IOException {
		_response.setContentType("text/html");
		PrintWriter printwriter = _response.getWriter();
		printwriter.print(_strcontent);
		printwriter.close();
	}

	   //redirect to the user to a webpage
	   public void redirectTo(String _strremoteaddress, String _strwebpage, String _strredirectto, HttpServletResponse _response) throws IOException {
		   _response.setHeader(_strremoteaddress, _strwebpage);
		   _response.setContentType("text/html");
		   _response.sendRedirect(_strredirectto);
	   }
	   
}

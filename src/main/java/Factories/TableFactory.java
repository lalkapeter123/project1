package Factories;

import java.util.List;
import javax.xml.crypto.Data;
import java.sql.SQLException;
import Data.CurrentUser;

import DatabaseCommands.DatabaseService;

public class TableFactory {

	String strtable = "";
	String strtableheader = "<table>\r\n";
	String strtablefooter = "</table>\r\n";
	DatabaseService databaseservice = new DatabaseService();
	CurrentUser currentuser;
	List<List<Object>> arrlstarrlstrecords = null;
	List<Object> arrlstrecord = null;

	String[] arrstrclaimsalignment = new String[] {null,"right",null,"center","center","right","right"};
	String [] arrstremployeeclaims = new String[]{null,"right",null,"center","center","right","right","center"};
	
	public TableFactory() {}
	
	
	public String createEmployeeTableView() {
		
		String stremployeetableview = "<table>"
		+ "   <tr class=\"employee_header2\"><td align=center colspan=2>Employee Name</td></tr>"
		+ "   <tr><td height=5 colspan=2></td></tr>"
		+ "   <tr><td align=right>First Name:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"FirstName\" value=\"" + currentuser.strfirstname  + "\" /></td></tr>"
		+ "   <tr><td class=\"tdr\">Last Name:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"LastName\" value = \"" + currentuser.strlastname + "\" /></td><tr><td align=center height=11pt colspan=2></td></tr>"
		+ "   <tr><tr><td class=\"employee_header2\" align=center colspan=2>Phone Numbers</td></tr>"
		+ "   <tr><td height=5 colspan=2></td></tr>"
		+ "   <tr><td class=\"tdr\">Work Phone Number:</td><td class=\"tdinput\"><input class=\"tdinput\" type=\"text\" name=\"WorkPhoneNumber\" size=30 value=\"" + currentuser.strworkphonenumber + "\"/></td></tr>"
		+ "   <tr><td class=\"tdr\">Home Phone Number:</td><td><input class=\"tdinput\" size=30 name=\"HomePhoneNumber\" value=\"" + currentuser.strhomephonenumber + "\"  /></td></tr>"
		+ "   <tr><td height=20 colspan=2></td></tr>"
		+ "   <tr><td align=center class=\"employee_header2\" colspan=2>Address</td></tr>"
		+ "   <tr><td height=5 colspan=2></td></tr>"
		+ "   <tr><td class=\"tdr\">Street 1:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street1\" value=\"" + currentuser.strstreet1 + "\"  /></td></tr>"
		+ "   <tr><td class=\"tdr\">Street 2:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street2\" value=\"" + currentuser.strstreet2 + "\"  /></td></tr>"
		+ "   <tr><td class=\"tdr\">City:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"City\" value=\"" + currentuser.strcity + "\"  /></td></tr>"
		+ "   <tr><td class=\"tdr\">State:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"State\" value=\"" + currentuser.strstate + "\"  /></td></tr>"
		+ "   <tr><td class=\"tdr\">Zip Code:</td><td class=\"tdinput\"><input size=30 type=\"text\" name=\"ZipCode\" value=\"" + currentuser.strzipcode + "\"  /></td></tr>"
		+ "   <tr><td height=20 colspan=2></td></tr>"
		+ "</table>";
		
		return stremployeetableview;
		
	}
	
	//manager pending claims
	public String createManagerChangePendingClaimsTable(String _strcommand) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords(_strcommand,null);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;		
		
	}
	
	//employee pending claims
	public String createManagerEmployeeChangePendingClaimsTable(String _strcommand, String _struserid) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords(_strcommand, _struserid);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;		
		
	}
	
	//claim headers
	public String getApproveRejectClaimHeaders() {
		
		String strheader = "<thead><tr style=\"font-weight:bold\">"
				+ "<td style=\"font-size:10pt;font-family:sans-serif\">Project ID</td>"
				+ "<td style=\"font-size:10pt;font-family:sans-serif\">Type</td>"
				+ "<td style=\"font-size:10pt;font-family:sans-serif\">Comments</td>"
				+ "<td align=center style=\"font-size:10pt;font-family:sans-serif\">Date</td>"
				+ "<td align=center style=\"font-size:10pt;font-family:sans-serif\">Expense Date</td>"
				+ "<td align=left style=\"font-size:10pt;font-family:sans-serif\">Reason</td>"
				+ "<td align=right style=\"font-size:10pt;font-family:sans-serif\">Amount</td>"
				+ "<td align=right style=\"font-size:10pt;font-family:sans-serif\">Approve</td>"
				+ "<td align=right style=\"font-size:10pt;font-family:sans-serif\">Reject</td>"
				+ "</tr></thead>";
		
		return strheader;
		
	}


	//get rows
	public String getRows(List<List<Object>> _arrlstarrlstobj) {
		
		String str = "";
		
		for(List<Object> arrlstobj : _arrlstarrlstobj) {
			str += "<tr>" + getRow(arrlstobj) + "</tr>\r\n";
		}
		
		return str;
		
	}	


	//get row
	public String getRow(List<Object> arrlstobj) {

		String str = "";
		for(int i = 1 ; i < arrlstobj.size() ; i++) {
			str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + arrlstobj.get(i) + "</td>\r\n";
		}
		str += "<td><a href=\"./ManagerChangePendingClaim.html?Action=Approve&id=" + arrlstobj.get(0) + "\"><img width=18 height=18 src=\"../images/Approve.jpg\" /></a></td>\r\n";
		str += "<td><a href=\"./ManagerChangePendingClaim.html?Action=Reject&id=" + arrlstobj.get(0) + "\"><img width=18 height=18 src=\"../images/Reject.jpg\" /</a></td>\r\n";
		return str;

	}


	//get records
	public List<List<Object>> getRecords(String _strcommand, String _struserid) throws SQLException {

		List<List<Object>> o = null;
		try {
			if(_struserid == null) {
				return (List<List<Object>>)new DatabaseService().getCommand(_strcommand,null);
			} else {
				return (List<List<Object>>)new DatabaseService().getCommand(_strcommand,new String[] {_struserid});
			}
		}catch (Exception e) {}
		
		return o;

	}
	
	
	//Reimbursement Claim Rows
	
	
	//all claims
	public String createReimbursementClaimsTable() throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view all claims", null);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;
		
	}
	
	//pending claims
	public String createPendingClaimsTable() throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view pending claims", null);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;
		
	}	
	
	//rejected claims
	public String createRejectedClaimsTable() throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view rejected claims", null);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;
		
	}	
	
	//approved claims
	public String createApprovedClaimsTable() throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view approved claims", null);
		
		strcontent += "<table>";
		strcontent += getApprovedClaimHeaders();
		strcontent += getApprovedClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;
		
	}	

	//employee approved claims
	public String createApprovedClaimsTable(String _strid) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view approved claims", _strid);
		
		strcontent += "<table>";
		strcontent += getApprovedClaimHeaders();
		strcontent += getApprovedClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;
		
	}	

	
	//employee claims
	public String createEmployeeReimbursementClaimsTable(String _str) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view employee claims",_str);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;
		
	}

	//employee pending claims
	public String createEmployeePendingClaimsTable(String _str) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view employee pending claims",_str);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;		
		
	}
	
	//employee approved claims
	public String createEmployeeApprovedClaimsTable(String _str) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view employee approved claims",_str);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;		
		
	}
	
	//employee rejected claims
	public String createEmployeeRejectedClaimsTable(String _str) throws SQLException {
		
		String strcontent = "";
		
		List<List<Object>> arrlstarrlstobj = getRecords("view employee rejected claims", _str);
		
		strcontent += "<table>";
		strcontent += getClaimHeaders();
		strcontent += getClaimRows(arrlstarrlstobj);
		strcontent += "</table>";
		
		return strcontent;		
		
	}	


	
	//APPROVED REQUESTS
	public String getApprovedClaimHeaders() {
		
		String strheader = "<thead align=center style=\"font-weight:bold;font-family:sans-serif;font-size:9pt\">"
				+ "<tr>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Project ID</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Type</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Comments</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Date</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Expense Date</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Reason</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Amount</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Manager</td>"
				+ "</tr>"
				+ "</thead>";
		
		return strheader;
		
	}
	
	//get records
	public String getApprovedClaimRows(List<List<Object>> _arrlstarrlstobj) {
		
		String str = "";
		
		for(List<Object> arrlstobj : _arrlstarrlstobj) {
			str += "<tr>" + getApprovedClaimRow(arrlstobj) + "</tr>\r\n";
		}
		
		return str;
		
	}	
	
	//get record
	public String getApprovedClaimRow(List<Object> arrlstobj) {
		String str = "";
		for(int i = 1 ; i < arrlstobj.size() ; i++) {
			if(i == 2) {
				String strtype = "";
				if(arrstremployeeclaims[i-1].compareTo("0") == 0){
					strtype = "Air Fare";
				}else if(arrstremployeeclaims[i-1].compareTo("1") == 1){
					strtype = "Car Travel";
				} else {
					strtype = "Moving Expense";
				}
				str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + strtype + "</td>\r\n";
			} else if(i==6) {
				String strreason = "";
				if(arrstremployeeclaims[i-1].compareTo("0") == 0){
					strreason = "Air Travel";
				} else if(arrstremployeeclaims[i-1].compareTo("1") == 1){
					strreason = "Car Travel";
				} else if(arrstremployeeclaims[i-1].compareTo("2") == 1){
					strreason = "Transport";
				} else {
					strreason = "Other Expense";
				}				
				str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + strreason + "</td>\r\n";
			} else {
				str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + arrlstobj.get(i) + "</td>\r\n";
			}
			
		}
		return str;
	}	
	
	public String getClaimHeaders() {
		
		String strheader = "<thead align=center style=\"font-weight:bold;font-family:sans-serif;font-size:9pt\">"
				+ "<tr>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Project ID</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Type</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Comments</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Date</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Expense Date</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Reason</td>"
				+ 	"<td style=\"font-family:sans-serif;font-size:9pt\">Amount</td>"
				+ "</tr>"
				+ "</thead>";
		
		return strheader;
		
	}
	
	//get records
	public String getClaimRows(List<List<Object>> _arrlstarrlstobj) {
		
		String str = "";
		
		for(List<Object> arrlstobj : _arrlstarrlstobj) {
			str += "<tr>" + getClaimRow(arrlstobj) + "</tr>\r\n";
		}
		
		return str;
		
	}	
	
	//get record
	public String getClaimRow(List<Object> arrlstobj) {
		String str = "";
		for(int i = 1 ; i < arrlstobj.size() ; i++) {
			if(i == 2) {
				String strtype = "";
				if(arrstremployeeclaims[i-1].compareTo("0") == 0){
					strtype = "Air Fare";
				}else if(arrstremployeeclaims[i-1].compareTo("1") == 1){
					strtype = "Car Travel";
				} else {
					strtype = "Moving Expense";
				}
				str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + strtype + "</td>\r\n";
			} else if(i==6) {
				String strreason = "";
				if(arrstremployeeclaims[i-1].compareTo("0") == 0){
					strreason = "Air Travel";
				} else if(arrstremployeeclaims[i-1].compareTo("1") == 1){
					strreason = "Car Travel";
				} else if(arrstremployeeclaims[i-1].compareTo("2") == 1){
					strreason = "Transport";
				} else {
					strreason = "Other Expense";
				}				
				str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + strreason + "</td>\r\n";
			} else {
				str += "<td style=\"font-family:sans-serif;font-size:9pt\" align=\"" + arrstremployeeclaims[i-1] + "\">" + arrlstobj.get(i) + "</td>\r\n";
			}
			
		}
		return str;
	}
	
	
	
	//EMPLOYEES	
	
	//EMPLOYEE TABLE
	
	//all employees
	public String createEmployeeTable() throws SQLException {
		String strtableheaderrow = getEmployeesTableHeader();
		arrlstarrlstrecords = (List<List<Object>>)databaseservice.getCommand("view all employees",null);
		String strtable = strtableheader;
		strtable += strtableheaderrow;
		strtable += getEmployeesTableRows(arrlstarrlstrecords);
		strtable += strtablefooter;
		
		System.out.println(strtable);
		
		return strtable;
	}
	
	//employee
	public String createEmployeeTable(Object _obj) throws SQLException {
		arrlstrecord = (List<Object>)databaseservice.getCommand("view employee",_obj);

		String strtableheaderrow = "<span style=\"font-family:sans-serif;font-size:15pt\">Profile for " + arrlstrecord.get(1) + " " + arrlstrecord.get(2) + "</span><br /><br />";
		String strtable = "<br /><br /><br />" + strtableheader;
		strtable += strtableheaderrow;
		strtable += getEmployeeTableRows(arrlstrecord);
		strtable += strtablefooter;
		return strtable;
	}

	//profile view
	public String getEmployeeTableRows(List<Object> _arrlstobj) {
		
		//add a change and remove link
		String strrows = "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">First Name: </td><td align=left>" + _arrlstobj.get(1) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Last Name: </td><td align=left>" + _arrlstobj.get(2) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Work Phone Number: </td><td align=left>" + _arrlstobj.get(3) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Home Phone Number: </td><td align=left>" + _arrlstobj.get(4) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Street1: </td><td align=left>" + _arrlstobj.get(5) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Street2: </td><td align=left>" + _arrlstobj.get(6) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Street3: </td><td align=left>" + _arrlstobj.get(7) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">City: </td><td align=left>" + _arrlstobj.get(8) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">State: </td><td align=left>" + _arrlstobj.get(9) + "</td></tr>\r\n";
		strrows += "<tr style=\"font-family:sans-serif;font-size:9pt\"><td align=right style=\"background:#EEEEEE\">Zip Code: </td><td align=left>" + _arrlstobj.get(10) + "</td></tr>\r\n</table>\r\n";
		return strrows;
		
	}


	//Get the employee table rows.
	public String getEmployeesTableRows(List<List<Object>> _arrlstarrlstobj) {
		String strrows = "";
		for(List<Object> arrlstobj : _arrlstarrlstobj) {
			strrows += getEmployeeTableRow(arrlstobj);
		}
		return strrows;
	}
	
	
	//table header row
	public String getEmployeesTableHeader() {
		
		String strheaderrow = "<thead>";
		strheaderrow += "<tr style=\"font-family:sans-serif;font-size:12pt;font-weight:bold\"><td colspan=10 align=center>All Employees</td></tr>";
		strheaderrow += "<tr style=\"font-family:sans-serif;font-size:10pt;font-weight:bold\">";
		strheaderrow += "<td align=left width=60pt style=\"font-family:sans-serif;min-width:60pt\">First Name</td>";
		strheaderrow += "<td align=left width=60pt style=\"font-family:sans-serif;min-width:60pt\">Last Name</td>";
		strheaderrow += "<td align=center width=110pt style=\"font-family:sans-serif;min-width:110pt\">Work Phone Number</td>";
		strheaderrow += "<td align=center width=110pt style=\"font-family:sans-serif;min-width:110pt\">Home Phone Number</td>";
		strheaderrow += "<td align=left width=100pt style=\"font-family:sans-serif;min-width:100pt\">Street1</td>";
		strheaderrow += "<td align=left width=100pt style=\"font-family:sans-serif;min-width:100pt\">Street2</td>";
		strheaderrow += "<td align=left width=100pt style=\"font-family:sans-serif;min-width:100pt\">Street3</td>";
		strheaderrow += "<td align=left width=65pt style=\"font-family:sans-serif;min-width:65pt\">City</td>";
		strheaderrow += "<td align=left width=40pt style=\"font-family:sans-serif;min-width:40pt\">State</td>";
		strheaderrow += "<td align=left width=50pt style=\"font-family:sans-serif;min-width:50pt\">Zip Code</td>";
		strheaderrow += "<td align=left width=50pt style=\"font-family:sans-serif;min-width:50pt\">View</td>";
		strheaderrow += "<td align=left width=50pt style=\"font-family:sans-serif;min-width:50pt\">Change</td>";
		strheaderrow += "</tr></thead>\r\n";
		return strheaderrow;
		
	}

	//Get the row for the employee table
	public String getEmployeeTableRow(List<Object> _arrlstobj) {

		String strrow = "<tr style=\"font-family:sans-serif;font-size:10pt\">";
		//add a change and remove link
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(1) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(2) + "</td>";
		strrow += "<td align=center style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(3) + "</td>";
		strrow += "<td align=center style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(4) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(5) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(6) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(7) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(8) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(9) + "</td>";
		strrow += "<td align=left style=\"font-family:sans-serif;font-size:9pt\">" + _arrlstobj.get(10) + "</td>";
		strrow += "<td><a href=\"../EmployeeChange/ManagerEmployeeView.html?id=" + _arrlstobj.get(0) + "\"><img height=16 width=16 src=\"../images/search.jpg\" /></a></td>\r\n";
		strrow += "<td><a href=\"../EmployeeChange/ManagerChangeEmployee.html?id=" + _arrlstobj.get(0) + "\"><img height=16 width=16 src=\"../images/change.jpg\" /></a></td>\r\n";
		strrow += "</tr>\r\n";
		return strrow;

	}

	
	
}

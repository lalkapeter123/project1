package Factories;

import DatabaseCommands.DatabaseService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WebPageFactory {

	DatabaseService databaseservice = new DatabaseService();

	Data.CurrentUser currentuser;

	public WebPageFactory() {}

	//opening tags
	public String getWebPageOpeningElements(String _strtitle) {
		return "<!DOCTYPE html>" + 
				"<html>" + 
				"<head>" + 
				"<meta charset=\"ISO-8859-1\">" + 
				"<title>" + _strtitle + "</title>" + 
				"<link rel=stylesheet href=\"/ExpenseWebsite/scripts/reimbursement.css\">" +
				"<link rel=\"preload\" href=\"../scripts/reimbursement.js\" as=\"script\">" + 
				"	</head>" + 
				"	<body>";
	}

	
	//EMPLOYEE VIEW WEB PAGES
	//employee choice form
	public String getEmployeeChoiceForm() throws SQLException {
		
		List<List<Object>> arrlstarrlstobj = (ArrayList<List<Object>>) databaseservice.getCommand("employee options",null);

		String stremployeechoiceform = "<form action=\"\" target=\"\" method=POST >\r\n";
		stremployeechoiceform += "<select name=\"employee\">\r\n";
		
		stremployeechoiceform += "<option value=\"" + -1 + "\">*</option>\r\n";
		
		for(Object obj : arrlstarrlstobj) {		
			List<Object> lstobj = (List<Object>)obj;			
			stremployeechoiceform += "<option value=\"" + lstobj.get(0) + "\">" + lstobj.get(1) + ", " + lstobj.get(2) + "</option>\r\n";
		}
		
		stremployeechoiceform += "</select>\r\n";
		stremployeechoiceform += "<input type=\"submit\" value=\"Update Table\" /><br /><br /><br />";
		stremployeechoiceform += "</form>";
		
		return stremployeechoiceform;
		
	}

	//The employee view web page.
	public String createViewEmployeeProfileWebPage(String _struserid) throws SQLException {
		
		List<Object> arrlstobject = (List<Object>)databaseservice.getCommand("view employee profile",new String[] {_struserid});
		
		String strwebpage = "\r\n\r\n<table class=\"manager_menu\">\r\n			"
		+ "<tr class=\"manager_header2\">"
		+ 	"<td align=center colspan=2>Employee Name</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>First Name:</td>"
		+ 	"<td>" + arrlstobject.get(1) + "</td></tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Last Name:</td>"
		+ 	"<td>" + arrlstobject.get(2) + "</td>"
		+ "</tr>"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr class=\"manager_header2\">"
		+ 	"<td align=center colspan=2>Phone Number</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Work Phone Number:</td>"
		+ 	"<td>" + arrlstobject.get(3) + "</td>"
		+ "</tr>"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Home Phone Number:</td>"
		+ 	"<td>" + arrlstobject.get(4) + "</td></tr>\r\n"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr class=\"manager_header2\">"
		+ 	"<td align=center colspan=2>Login</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>User Name:</td>"
		+ 	"<td>" + arrlstobject.get(11) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Password:</td>"
		+ 	"<td>" + arrlstobject.get(12) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr class=\"manager_header2\">"
		+ 	"<td align=center colspan=2>Address</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Street 1:</td>"
		+ 	"<td>" + arrlstobject.get(5) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Street 2:</td>"
		+ 	"<td>" + arrlstobject.get(6) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>City:</td>"
		+ 	"<td>" + arrlstobject.get(8) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>State:</td>"
		+ 	"<td>" + arrlstobject.get(9) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr class=\"tdr\">"
		+ 	"<td>Zip Code:</td>"
		+ 	"<td>" + arrlstobject.get(10) + "</td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "\r\n			</table>\r\n\r\n";
		
		return strwebpage;
		
	}


	//Removed employee message
	public String removedEmployee(String _stremployee) {

		return "<br /><br /><span style=\"font-family:sans-serif\">Removed Employee " + _stremployee + "</span>";
		
	}
	
	
	//The change employee form.
	public String createChangeEmployeeForm(String _string) throws SQLException {
		
		List<Object> arrlstobject = (List<Object>)databaseservice.getCommand("view employee profile",new String[] {_string});

		String strwebpage = "<form action=\"\" target=\"\" method=\"POST\">\r\n\r\n"
		+ "<table class=\"tdl\">\r\n			"
		+ "<tr><td><input name=\"UserID\" value=\"" + arrlstobject.get(0) + "\" style=\"visibility:hidden\"/></td></tr>\r\n"
		+ "<tr class=\"employee_header2\">"
		+ 	"<td align=center colspan=2>Employee Name</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td align=right>First Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"FirstName\" value=\"" + arrlstobject.get(1) + "\" /></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Last Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"LastName\" value = \"" + arrlstobject.get(2) + "\" /></td>"
		+ "<tr>"
		+ 	"<td align=center height=11pt colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ "<tr>"
		+ 	"<td class=\"employee_header2\" align=center colspan=2>Phone Numbers</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Work Phone Number:</td>"
		+ 	"<td class=\"tdinput\"><input class=\"tdinput\" type=\"text\" name=\"WorkPhoneNumber\" size=30 value=\"" + arrlstobject.get(3) + "\"/></td>"
		+ "</tr>"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Home Phone Number:</td>"
		+ 	"<td><input class=\"tdinput\" size=30 name=\"HomePhoneNumber\" value=\"" + arrlstobject.get(4) + "\"  /></td></tr>\r\n"
		+ "<tr>"
		+ 	"<td height=20 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td align=center class=\"employee_header2\" colspan=2>Login</td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">User Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"UserName\" value=\"" + arrlstobject.get(11) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Password:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Password\" value=\"" + arrlstobject.get(12) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=20 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td align=center class=\"employee_header2\" colspan=2>Address</td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Street 1:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street1\" value=\"" + arrlstobject.get(5) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Street 2:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street2\" value=\"" + arrlstobject.get(6) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">City:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"City\" value=\"" + arrlstobject.get(8) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">State:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"State\" value=\"" + arrlstobject.get(9) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Zip Code:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"ZipCode\" value=\"" + arrlstobject.get(10) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=20 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr>\r\n			   "
		+ 	"<td class=\"tdr\"></td>\r\n			   "
		+ 	"<td align=\"leftclass=\"tdinput\">\r\n<input size=30 type=\"submit\" value=\"Change Employee\" style=\"font-size:15pt;background:white;border-color:Orange\" />\r\n			   </td>\r\n			"
		+ "</tr>\r\n			</table>\r\n\r\n			</form>";
		
		return strwebpage;
		
	}

	//The change employee form.
	public String createManagerChangeEmployeeForm(String _string) throws SQLException {
		
		List<Object> arrlstobject = (List<Object>)databaseservice.getCommand("view employee profile",new String[] {_string});

		String strwebpage = "<form action=\"\" target=\"\" method=\"POST\">\r\n\r\n"
		+ "<table class=\"tdl\">\r\n			"
		+ "<tr><td><input name=\"UserID\" value=\"" + arrlstobject.get(0) + "\" style=\"visibility:hidden\"/></td></tr>\r\n"
		+ "<tr class=\"manager_header2\">"
		+ 	"<td align=center colspan=2>Employee Name</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td align=right>First Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"FirstName\" value=\"" + arrlstobject.get(1) + "\" /></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Last Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"LastName\" value = \"" + arrlstobject.get(2) + "\" /></td>"
		+ "<tr>"
		+ 	"<td align=center height=11pt colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ "<tr>"
		+ 	"<td class=\"manager_header2\" align=center colspan=2>Phone Numbers</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Work Phone Number:</td>"
		+ 	"<td class=\"tdinput\"><input class=\"tdinput\" type=\"text\" name=\"WorkPhoneNumber\" size=30 value=\"" + arrlstobject.get(3) + "\"/></td>"
		+ "</tr>"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Home Phone Number:</td>"
		+ 	"<td><input class=\"tdinput\" size=30 name=\"HomePhoneNumber\" value=\"" + arrlstobject.get(4) + "\"  /></td></tr>\r\n"
		+ "<tr>"
		+ 	"<td height=20 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td align=center class=\"manager_header2\" colspan=2>Login</td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">User Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"UserName\" value=\"" + arrlstobject.get(11) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Password:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Password\" value=\"" + arrlstobject.get(12) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=20 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td align=center class=\"manager_header2\" colspan=2>Address</td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Street 1:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street1\" value=\"" + arrlstobject.get(5) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Street 2:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street2\" value=\"" + arrlstobject.get(6) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">City:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"City\" value=\"" + arrlstobject.get(8) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">State:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"State\" value=\"" + arrlstobject.get(9) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Zip Code:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"ZipCode\" value=\"" + arrlstobject.get(10) + "\"  /></td>"
		+ "</tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=20 colspan=2></td>"
		+ "</tr>\r\n			"
		+ "<tr>\r\n			   "
		+ 	"<td class=\"tdr\"></td>\r\n			   "
		+ 	"<td align=\"leftclass=\"tdinput\">\r\n<input size=30 type=\"submit\" value=\"Change Employee\" style=\"font-size:15pt;background:white;border-color:rgb(155, 114, 155)\" />\r\n			   </td>\r\n			"
		+ "</tr>\r\n			</table>\r\n\r\n			</form>";
		
		return strwebpage;
		
	}

	//The add employee web page.
	public String createAddEmployeeWebPage() {
		
		String strform = "<form action=\"\" target=\"\" method=\"POST\">\r\n\r\n			<table class=\"tdl\">\r\n			"
		+ "<tr>"
		+ 	"<td class=\"manager_header2\" align=center colspan=2>Employee Name</td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">First Name:</td>"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"FirstName\" /></td></tr>\r\n			"
		+ "<tr>"
		+ 	"<td class=\"tdr\">Last Name:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"LastName\" /></td>\r\n"
		+ "</tr>\r\n"
		+ "<tr>\r\n"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>\r\n"
		+ "<tr>\r\n"
		+ 	"<td class=\"manager_header2\" align=center colspan=2>Phone Numbers</td></tr>\r\n			"
		+ "</tr>\r\n"
		+ "<tr>\r\n"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>\r\n"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">Work Phone Number:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input class=\"tdinput\" type=\"text\" name=\"WorkPhoneNumber\" size=30 /></td>\r\n"
		+ "</tr>\r\n"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">Home Phone Number:</td\r\n>"
		+ 	"<td><input class=\"tdinput\" size=30 name=\"HomePhoneNumber\" /></td></tr>\r\n"
		+ "<tr>\r\n"
		+ "<tr>\r\n"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td align=center class=\"manager_header2\" colspan=2>Login</td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td height=5 colspan=2></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">User Name:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"UserName\" /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">Password:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Password\"  /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td align=center class=\"manager_header2\" colspan=2>Address</td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td height=5 colspan=2></td></tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">Street 1:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street1\" /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">Street 2:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"Street2\" /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">City:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"City\" /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">State:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"State\" /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td class=\"tdr\">Zip Code:</td>\r\n"
		+ 	"<td class=\"tdinput\"><input size=30 type=\"text\" name=\"ZipCode\" /></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n"
		+ 	"<td height=20 colspan=2></td>\r\n"
		+ "</tr>\r\n			"
		+ "<tr>\r\n			   "
		+ 	"<td class=\"tdr\"></td>\r\n			   "
		+ 	"<td align=\"leftclass=\"tdinput\">\r\n<input size=30 type=\"submit\" value=\"Add Employee\" style=\"background:white;font-size:15pt;border-color:Purple\" />\r\n"
		+ "			   </td>\r\n			"
		+ "</tr>\r\n"
		+ "			</table>\r\n\r\n"
		+ "			</form>";
		
		return strform;
		
	}

	
	//WEB PAGE HEADERS

	//The employee web page header.
	public String createEmployeeHeader() {
		
		return getWebPageOpeningElements("Employee") +	
		"	<div id=\"profileDiv\" onpointerout=\"hideEmployeeProfile()\" onpointerover=\"showEmployeeProfile()\" style=\"font-size:4pt;text-align:center;position:absolute;visibility:hidden;background:white;width:90pt;height:20pt\">\r\n" +
		"	    <br />\r\n" + 
		"	    <div onclick=\"editProfile()\" id=\"profileDivLink\" style=\"background:white;font-size:11pt;text-align:center;font-family:sans-serif\">\r\n" + 
		"	        <img src=\"../images/Profile.jpg\"/>\r\n" + 
		"	    </div>\r\n" + 
		"	</div>\r\n\r\n" + 
	
		"	<!-- The opening header table with a link to the employee portal and the manager portal. -->\r\n" + 
		"	<table cellspacing=0 id=\"t1\" width=\"100%\">\r\n" + 
		"	<tr class=\"webpage_header_menu\">\r\n" + 
		"	    <td id=\"employee\" colspan=2><img src=\"/ExpenseWebsite/images/revatureexpense.jpg\" /></td>\r\n" + 
		"	    <td style=\"text-align:right;font-family:sans-serif\" colspan=4>\r\n" +
		"	        <span style=\"font-family:sans-serif;font-size:11pt;text-align:right\" id=\"p1\"></span>\r\n" + 
		"	    </td>\r\n" + 
		"	</tr>\r\n" +
		"	<tr class=\"webpage_header_menu\">\r\n" + 
		"		<!-- replace employee label with employee name-->\r\n" + 
		"		<td class=\"tdr\" colspan=3 style=\"width:40pt\"></td>\r\n" + 
		"	    <td width=100 style=\"min-width:100pt\">\r\n" + 
		"	        <div class=\"tdr\" align=right onpointerout=\"hideEmployeeProfile()\" onpointerover=\"showEmployeeProfile()\" id=\"employeediv\">Hello " + Data.CurrentUser.strfirstname + " " + Data.CurrentUser.strlastname + "</div>\r\n" + 
		"	    </td>\r\n" + 
		"	    <td style=\"width:7pt\">/</td>\r\n" + 
		"	    <td class=\"tdl\" style=\"width:60pt\"><a href=\"/ExpenseWebsite/logoff\">Logoff</a></td>\r\n" + 
		"	</tr>\r\n" + 
		"	<tr class=\"tdl\" style=\"background:Orange\">\r\n" +
		"	    <td width=80pt style=\"min-width:80pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/EmployeeReimbursementClaims.html\">All</a></td>\r\n" +
		"       <td colspan=1 align=left style=\"font-family:sans-serif\"><a href=\"/ExpenseWebsite/Claim/EmployeeAddReimbursement.html\">Add Claim</a></td>\r\n" +
		"       <td colspan=4 width=200pt style=\"min-width=200pt\"><a style=\"font-family:sans-serif\" href=\"/ExpenseWebsite/UpdateInformation\">Update Personal Information</a></td>\r\n" + 
		"   </tr>\r\n" + 
		"	<tr class=\"tdl\" style=\"background:Orange\">\r\n" +
		"       <td width=80pt style=\"min-width:80pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/EmployeePendingReimbursements.html\">Pending</a></td>\r\n" +
		"       <td colspan=5 ></td>\r\n" + 
		"   </tr>\r\n" + 
		"	<tr class=\"tdl\" style=\"background:Orange\">\r\n" +
		"	    <td width=80pt style=\"min-width:80pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/EmployeeRejectedReimbursements.html\">Rejected</a></td>\r\n" +
		"       <td colspan=5 ></td>" + 
		"   </tr>" + 
		"	<tr class=\"tdl\" style=\"background:Orange\">" +
		"	    <td width=80pt style=\"min-width:80pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/EmployeeApprovedReimbursements.html\">Approved</a></td>\r\n" +
		"       <td colspan=5 align=left></td>\r\n" + 
		"   </tr>\r\n" + 
		"   <tr class=\"tdl\" style=\"background:Orange\"></tr>\r\n" +
		"</table>\r\n"
		+ "<script>\r\n"
		+ "  var date = new Date();\r\n"
		+ "   document.getElementById(\"p1\").innerHTML = date.getMonth() + \"/\" + date.getDate() + \"/\" + date.getFullYear();\r\n"
		+ "</script>\r\n";
		
	}

	//The manager web page header.
	public String createManagerWebPageHeader() {
		return getWebPageOpeningElements("Manager")
				+ "		<div id=\"profileDiv\" onpointerout=\"hideManagerProfile()\" onpointerover=\"showManagerProfile()\" style=\"font-size:4pt;text-align:center;position:absolute;visibility:hidden;background:white;width:90pt;height:20pt\">\r\n"
				+ "		    <br />\r\n"
				+ "         <!-- Hidden div for the profile change link -->\r\n"
				+ "		    <div onclick=\"editProfile()\" id=\"profileDivLink\">\r\n"
				+ "		        <img src=\"../images/Profile.jpg\"/>\r\n"
				+ "		    </div>\r\n"
				+ "		</div>\r\n"
				+ "		<!-- The opening header table with a link to the manager portal and the manager portal. -->\r\n"
				+ "		<table cellspacing=0 id=\"t1\" width=\"100%\">\r\n"
				+ "		<tr class=\"webpage_header_menu\">\r\n"
				+ "		    <td id=\"manager\" colspan=3><img src=\"/ExpenseWebsite/images/revatureexpense.jpg\" /></td>\r\n"
				+ "		    <td style=\"text-align:right\" colspan=4>\r\n"
				+ "		        <span style=\"font-family:sans-serif;font-size:11pt;text-align:right\" id=\"p1\"></span>\r\n"
				+ "		    </td>\r\n"
				+ "		</tr>\r\n"
				+ "		<tr class=\"manager_menu_topright\">\r\n"
				+ "		<!-- replace manager label with manager name-->\r\n"
				+ "			<td width=150pt style=\"min-width:150pt\"></td>\r\n"
				+ "         <td></td>\r\n"
				+ "		    <td>\r\n"
				+ "		        <div align=right onpointerout=\"hideManagerProfile()\" onpointerover=\"showManagerProfile()\" id=\"managerdiv\">Manager</div>\r\n"
				+ "         </td>\r\n\r\n"
				+ "		   	<td width=\"7pt\">/</td>\r\n"
				+ "			<td width=\"60pt\"><a href=\"/ExpenseWebsite/logoff\">Logoff</a></td>\r\n"
				+ "		</tr>\r\n\r\n"
				+ "		<tr class=\"manager_header2\" >\r\n"
				+ "        <td width:150pt style=\"min-width:150pt;max-width:150pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/ManagerReimbursementClaims.html\">All Claims</a></td>\r\n"
				+ "        <td colspan=4 width:150pt style=\"min-width:150pt;max-width:150pt\" align=left><a href=\"/ExpenseWebsite/EmployeeView/Employees.html\">View Employees</a></td>\r\n" 
				+ "     </tr>\r\n"
				+ "     <tr class=\"manager_header2\" >\r\n"
				+ "         <td width:150pt style=\"min-width:150pt;max-width:150pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/ManagerPendingClaims.html\">Pending Claim</a></td>\r\n"
				+ "         <td align=left colspan=4><a href=\"/ExpenseWebsite/EmployeeChange/ManagerAddEmployee.html\">Add Employee</a></td>\r\n"
				+ "     </tr>\r\n"
				+ "     <tr class=\"manager_header2\" >\r\n"
				+ "         <td width:150pt style=\"min-width:150pt;max-width:150pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/ManagerRejectedClaims.html\">Rejected Claims</a></td>\r\n"
				+ "         <td colspan=4 width:150pt style=\"min-width:150pt;max-width:150pt\"></td>\r\n"
				+ "     </tr>\r\n"
				+ "     <tr class=\"manager_header2\" >\r\n"
				+ "         <td width:150pt style=\"min-width:150pt;max-width:150pt\"><a href=\"/ExpenseWebsite/ViewReimbursements/ManagerApprovedClaims.html\">Approved Claims</a></td>\r\n"
				+ "         <td colspan=4 width:150pt style=\"min-width:150pt;max-width:150pt\"></td>\r\n"
				+ "     </tr>\r\n"
				+ "     <tr class=\"manager_header2\" >\r\n"
				+ "        <td width:150pt style=\"min-width:150pt;max-width:150pt\"><a href=\"/ExpenseWebsite/Claim/ManagerChangePendingClaim.html\">Approve/Reject Claims</a></td>\r\n"
				+ "        <td colspan=4 width:150pt style=\"min-width:150pt;max-width:150pt\"></td>\r\n"
				+ "     </tr>\r\n"
				+ "		</table>\r\n"
				+ "<script>\r\n"
				+ "  var date = new Date();\r\n"
				+ "   document.getElementById(\"p1\").innerHTML = date.getMonth() + \"/\" + date.getDate() + \"/\" + date.getFullYear();\r\n"
				+ "</script>\r\n";
	}

}

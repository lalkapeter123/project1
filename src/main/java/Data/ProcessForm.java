package Data;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import DatabaseCommands.DatabaseService;

public class ProcessForm {
	
	public ProcessForm() {}
	
	//add employee form
	public Object processAddEmployee(String [] _arrstr) {
	
		Object o = null;
		
    	try {
    		new DatabaseService().getCommand("add employee",_arrstr);
    	} catch(SQLException se) {
    		se.printStackTrace();
    	}
    	
    	return o;
    	
	}
	
	//change employee form
	public Object processChangeEmployee(String[] _arrstr) {
		
		Object o = null;
		
    	try {
    		o = new DatabaseService().getCommand("change employee",_arrstr);
    	} catch(SQLException se) {
    		se.printStackTrace();
    	}
    	
    	return o;
    	
	}
	
}

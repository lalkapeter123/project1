package Data;

import java.util.List;

//The purpose of this class is to keep an updated list of user information for display and logon status.
//This can be can be an instantiated class that doesn't use static variables.
public class CurrentUser {
	
	//private static final Logger logger = LogManager.getLogger(CurrentUser.class);
	
	//user id
	//this is the database user id for the user
	//when the user logs on this is set
	public static int intuserid = -1;
	//session id
	//this is the database table unique session id
	//when the user logs on this is set
	public static int intsessionid = -1;
	//ip address
	//this address is the session identifier
	//when a user logs off this is reset to an empty string
	//if it is empty the user must logon again
	public static String strremoteipaddress = "";
	
	//first name
	public static String strfirstname = "";
	//last name
	public static String strlastname = "";
	//work phone number
	public static String strworkphonenumber = "";
	//home home number
	public static String strhomephonenumber = "";
	//street1
	public static String strstreet1 = "";
	//street2
	public static String strstreet2 = "";
	//city
	public static String strcity = "";
	//state
	public static String strstate = "";
	//zip code
	public static String strzipcode = "";	
	
	
	//set the user information variables
	public static void setUserInformationVariables(List<Object> _arrlstobj) {
		
		//logger.trace("We're updating the user information from parameters!");
		
		strfirstname = (String)_arrlstobj.get(0);
		strlastname = (String)_arrlstobj.get(1);
		strworkphonenumber = (String)_arrlstobj.get(2);
		strhomephonenumber = (String)_arrlstobj.get(3);
		strstreet1 = (String)_arrlstobj.get(4);
		strstreet2 = (String)_arrlstobj.get(5);
		strcity = (String)_arrlstobj.get(7);
		strstate = (String)_arrlstobj.get(8);
		strzipcode = (String)_arrlstobj.get(9);
	}
	
	public static void setUserInformationVariablesFromParameters(List<String> _arrlstobj) {
		
		//logger.trace("We're updating the user information variables from parameters!");
		
		strfirstname = (String)_arrlstobj.get(3);
		strlastname = (String)_arrlstobj.get(4);
		strworkphonenumber = (String)_arrlstobj.get(5);
		strhomephonenumber = (String)_arrlstobj.get(6);
		strstreet1 = (String)_arrlstobj.get(7);
		strstreet2 = (String)_arrlstobj.get(8);
		strcity = (String)_arrlstobj.get(9);
		strstate = (String)_arrlstobj.get(10);
		strzipcode = (String)_arrlstobj.get(11);
	}
	
	//USER ID
	public static int getUserID() {
		return intuserid;
	}
	public static void setUserID(int _intuserid) {
		intuserid = _intuserid;
	}
	
	//SESSION ID
	public static int getSessionID() {
		return intsessionid;
	}
	public static void setSessionID(int _intsessionid) {
		intsessionid = _intsessionid;
	}
	
	//REMOTE IP ADDRESS
	public static String getRemoteIPAddress() {
		return strremoteipaddress;
	}
	public static void setRemoteIPAddress(String _strremoteipaddress) {
		strremoteipaddress = _strremoteipaddress;
	}

}

package Data;

import javax.servlet.http.HttpServletRequest;

public class ParameterHandler {
	
	public ParameterHandler() {}
	
	public String[] getEmployeeInformation(HttpServletRequest _request) {
		
    	String strusername = _request.getParameter("UserName");
    	String strpassword = _request.getParameter("Password");
    	String strfirstname = _request.getParameter("FirstName");
    	String strlastname = _request.getParameter("LastName");
    	String strworkphonenumber = _request.getParameter("WorkPhoneNumber");
    	String strhomephonenumber = _request.getParameter("HomePhoneNumber");
    	String strstreet1 = _request.getParameter("Street1");
    	String strstreet2 = _request.getParameter("Street2");
    	String strcity = _request.getParameter("City");
    	String strstate = _request.getParameter("State");
    	String strzipcode = _request.getParameter("ZipCode");
    	
    	return new String[] {strusername,strpassword,strfirstname,strlastname,strworkphonenumber,strhomephonenumber,strstreet1,strstreet2,strcity,strstate,strzipcode};

	}
	
	public String[] getEmployeeProfile(HttpServletRequest _request) {
		
    	String struserid = _request.getParameter("UserID");
    	String strusername = _request.getParameter("UserName");
    	String strpassword = _request.getParameter("Password");
    	String strfirstname = _request.getParameter("FirstName");
    	String strlastname = _request.getParameter("LastName");
    	String strworkphonenumber = _request.getParameter("WorkPhoneNumber");
    	String strhomephonenumber = _request.getParameter("HomePhoneNumber");
    	String strstreet1 = _request.getParameter("Street1");
    	String strstreet2 = _request.getParameter("Street2");
    	String strcity = _request.getParameter("City");
    	String strstate = _request.getParameter("State");
    	String strzipcode = _request.getParameter("ZipCode");
		
    	return new String[] {struserid,strusername,strpassword,strfirstname,strlastname,strworkphonenumber,strhomephonenumber,strstreet1,strstreet2,strcity,strstate,strzipcode};

	}
	
	public String[] getManagerClaimChangeParameters(HttpServletRequest _request) {
		
		String [] arrstr = null;
		
		String straction = _request.getParameter("Action");
		String struserid = _request.getParameter("id");
		
		arrstr = new String[] {straction,struserid};
		
		return arrstr;
		
	}
	
	
	public String[] getClaimChangeParameters(HttpServletRequest _request) {
		
		String [] arrstr = null;
		
		String strproject = _request.getParameter("project");

		String strtype = _request.getParameter("type");
		
		if(strtype.compareTo("Airfare") == 0){
			strtype = "air-travel";
		} else if(strtype.compareTo("Car Travel") == 0){
			strtype = "car-travel";
		} else if(strtype.compareTo("Relocation") == 0){
			strtype = "relocation";
		}

		String strreason = _request.getParameter("reason");
		
		if(strreason.compareTo("air-travel-to-project") == 0) {
			strreason = "Air-Travel-to-Project";
		} else if(strreason.compareTo("road-travel-to-project") == 0) { 
			strreason = "Car-Travel-to-Project";		
		} else if (strreason.compareTo("moving-expense") == 0) {
			strreason = "Transport";
		} else if (strreason.compareTo("other-expense") == 0) {
			strreason = "Other-Expense";
		}
		
		String strdate = _request.getParameter("date");
		String stramount = _request.getParameter("amount");
		
		arrstr = new String[] {strproject,strtype,strreason,strdate,stramount};
		
		return arrstr;
		
	}

}

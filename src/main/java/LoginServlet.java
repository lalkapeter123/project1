//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import java.io.*;
import java.sql.SQLException;
import java.util.List;
import java.util.Arrays;

import javax.servlet.*;
import javax.servlet.http.*;

import Data.ParameterHandler;
import DatabaseCommands.DatabaseService;
import Factories.TableFactory;
import Factories.WebPageFactory;

//this servlet handles the login and employee profile update
public class LoginServlet extends HttpServlet {

//private static final Logger logger = LogManager.getLogger(CurrentUser.class);
	
   //log in attempts
   int intlogonattempts = 3;
   String strusernamelogin = "";

   //database service object
   DatabaseService databaseService = null;
   
   //web page factory
   WebPageFactory webpagefactory = new WebPageFactory();
   //parameter handler
   ParameterHandler parameterhandler = new ParameterHandler();
 
   public void init() throws ServletException {}   
   
   //GET
   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
	 
	  //if there is a current session for the ip address
	  if(Data.CurrentUser.strremoteipaddress.length() != 0) {
	   
	      //referrer
	      String strwebpage = request.getRequestURL().toString();
	      
		  //System.out.println("------------------GET------" + strwebpage);
	      
	      //update information
		  if(strwebpage.endsWith("UpdateInformation")){
			  
			  String strresponse = "";
			  
			  try {
				  	String stremployeeviewheader = webpagefactory.createEmployeeHeader();
				  	strresponse = webpagefactory.createChangeEmployeeForm(String.valueOf(Data.CurrentUser.intuserid));
					String stremployeeviewfooter = "</body>\r\n</html>";
					printHTML(stremployeeviewheader + strresponse + stremployeeviewfooter, response);
				} catch(SQLException se) {
					se.printStackTrace();
				}
		  	}
      //if there is no remote ip address
      //redirect to the index.html page
	  } else {
    	  redirectTo(request.getRemoteAddr(), request.getRequestURL().toString(), "http://localhost:8080/ExpenseWebsite/index.html", response);
	  }
      
    }
   
  
   
   
   //POST
   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	  
	   
	   String strwebpage = request.getHeader("referer");
	   
	   //System.out.println("Webpage--------------------" + strwebpage);
		  
	   //The session is over and there is no remote ip address.
	   if(Data.CurrentUser.strremoteipaddress.length() == 0) {

	      //EMPLOYEE LOGIN		  
		  if(strwebpage.endsWith("EmployeeLogin.html") || strwebpage.endsWith("EmployeeFailedLogin.html")) {
	    	   
			//user name
			String strusername = request.getParameter("username");
			//password
			String strpassword = request.getParameter("password");
			//remote address
			String strremoteaddress = request.getRemoteAddr();
	          
	    	if(strusernamelogin.length() == 0 || strusername != null) {
	    		strusernamelogin = strusername;    		
	    	} else if(strusernamelogin.compareTo(strusername) != 0) {
	    		strusernamelogin = null;
	    	}	    	
	    	
	    	try {
	    		 
	    		//get the session id and call the logon user function
				Object o = new DatabaseService().getCommand("logon user",new String[] {strusername,strpassword,strremoteaddress});
				Data.CurrentUser.intsessionid = (Integer)o;
				
				//get the user id and call the get user id function
				o = new DatabaseService().getCommand("get user id",new String[] {strusername,strpassword});
				Data.CurrentUser.intuserid = (Integer)o;
				
				//System.out.println("-----------------------User ID : " + Data.CurrentUser.intuserid + "----------------------");
				
				//if the logon is successful
				if(Data.CurrentUser.intuserid != -1) {

					//set the remote ip address
					Data.CurrentUser.strremoteipaddress = request.getHeader("referer");
				    
					//test for a locked user in the database
					Object o1 = new DatabaseService().getCommand("user locked",new String[] {String.valueOf(Data.CurrentUser.intuserid),strremoteaddress,strwebpage});
					
					//if the user is not locked out
					if(((Integer)o1).toString().compareTo("1") == 0) {
						
						//get the user information
						List<Object> arrlstobjects = (List<Object>)new DatabaseService().getCommand("view employee",new Object[] {String.valueOf(Data.CurrentUser.intuserid)});
						
						//set the user information
						Data.CurrentUser.setUserInformationVariables(arrlstobjects);
						
						String stremployeeheader = webpagefactory.createEmployeeHeader();
						String stremployeefooter = "</body></html>";
						
						//log the traffic
						new DatabaseService().getCommand("add traffic",new String[] {((Integer)o).toString(),strwebpage});
						
						//print the employee information
						printHTML(stremployeeheader + stremployeefooter, response);
						
					//if the user is locked out go to the employee locked out web page
					} else {
						redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/EmployeeLockedOut.html", response);
					}
					
				//failed logon page
				} else {
					
					Data.CurrentUser.intsessionid = -1;
					Data.CurrentUser.intuserid = -1;
					
					intlogonattempts--;
	
					//redirect to the failed login page if the count of login attempts is greater than 0
					if(intlogonattempts > 0) {
						//System.out.println("-------------------Failed Login----------------------");
						redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/EmployeeFailedLogin.html", response);
					//redirect to the locked out employee page
					} else {
						//System.out.println("-------------------Locked Out----------------------");
						//reset the login attempts variable to 3
						intlogonattempts = 3;
						//call the function to lock out the user.
						new DatabaseService().getCommand("lock out user",new String[] {strusernamelogin,strremoteaddress,strwebpage});
						//redirect
						redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/EmployeeLockedOut.html", response);
					}
				}
	
	    	} catch (SQLException e) {
				e.printStackTrace();
			}
			
	    //MANAGER LOGIN			
		} else if(strwebpage.endsWith("ManagerLogin.html") || strwebpage.endsWith("ManagerFailedLogin.html")) {
	
			//System.out.println("------------------manager login");
	    	   
			//user name
			String strusername = request.getParameter("username");
			//password
			String strpassword = request.getParameter("password");
			//remote address
			String strremoteaddress = request.getRemoteAddr();
	          
	    	if(strusernamelogin.length() == 0 || strusername != null) {
	    		strusernamelogin = strusername;    		
	    	} else if(strusernamelogin.compareTo(strusername) != 0) {
	    		strusernamelogin = null;
	    	}	    	
	    	
	    	try {
	    		 
	    		//get the session id
				Object o = new DatabaseService().getCommand("logon manager",new String[] {strusername,strpassword,strremoteaddress});
				Data.CurrentUser.intsessionid = (Integer)o;
				
				//get the user id
				o = new DatabaseService().getCommand("get manager id",new String[] {strusername,strpassword});
				Data.CurrentUser.intuserid = (Integer)o;
				
				//System.out.println("-----------------------User ID : " + Data.CurrentUser.intuserid + "----------------------");
				
				//if the logon is successful
				if(Data.CurrentUser.intuserid != -1) {

					//set the remote ip address
					Data.CurrentUser.strremoteipaddress = request.getHeader("referer");
				      
					Object o1 = new DatabaseService().getCommand("manager locked",new String[] {String.valueOf(Data.CurrentUser.intuserid),strremoteaddress,strwebpage});
					
					//if the user is not locked out
					if(((Integer)o1).toString().compareTo("1") == 0) {
						
						String strloginheader = webpagefactory.createManagerWebPageHeader();
						String strloginfooter = "</body></html>";
						
						//log the traffic
						new DatabaseService().getCommand("add traffic",new String[] {((Integer)o).toString(),strwebpage});
						
						//print the employee information
						printHTML(strloginheader + strloginfooter, response);
						
					//if the user is locked out
					} else {
						redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/ManagerLockedOut.html", response);
					}
					
				//failed logon page
				} else {
					
					Data.CurrentUser.intsessionid = -1;
					Data.CurrentUser.intuserid = -1;
					
					intlogonattempts--;
	
					//redirect to the failed login page if the count of login attempts is greater than 0
					if(intlogonattempts > 0) {
						//System.out.println("-------------------Failed Login----------------------");
						redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/ManagerFailedLogin.html", response);
					//redirect to the locked out employee page
					} else {
						//System.out.println("-------------------Locked Out----------------------");
						//reset the login attempts variable to 3
						intlogonattempts = 3;
						//call the function to lock out the user.
						new DatabaseService().getCommand("lock out user",new String[] {strusernamelogin,strremoteaddress,strwebpage});
						//redirect
						redirectTo(strremoteaddress, strwebpage, "http://localhost:8080/ExpenseWebsite/Login/ManagerLockedOut.html", response);
					}
				}
	
	    	} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}//if
	

		  
	   //THERE IS A SESSION
	   //The session has started and there is a remote ip address.
	   } else {   
	   
		  //System.out.println("------------------update information");		   
		   
		  if(strwebpage.endsWith("UpdateInformation")){
			
			  try {
				  
		    	String [] arrstr = parameterhandler.getEmployeeProfile(request);
		    	new Data.ProcessForm().processChangeEmployee(arrstr);
		    	
		    	Data.CurrentUser.setUserInformationVariablesFromParameters(Arrays.asList(arrstr));
			  
				String strloginheader = webpagefactory.createEmployeeHeader();
				String stremployeeinformation = new TableFactory().createEmployeeTable();
				String strloginfooter = "</body></html>";
						
				printHTML(strloginheader + stremployeeinformation + strloginfooter, response);
			  
	    	} catch (SQLException e) {
				e.printStackTrace();
			}
		  }
	   }	
   }//function

   public void destroy() {}
   
   //redirect to the user to a webpage
   public void redirectTo(String _strremoteaddress, String _strwebpage, String _strredirectto, HttpServletResponse _response) throws IOException {
	   _response.setHeader(_strremoteaddress, _strwebpage);
	   _response.setContentType("text/html");
	   _response.sendRedirect(_strredirectto);
   }
   
   //print the html to output
   public void printHTML(String _strwebpage, HttpServletResponse response) throws IOException {
	   
		response.setContentType("text/html");
		PrintWriter printwriter = response.getWriter();
		printwriter.print(_strwebpage);
		printwriter.close();
		
   }
   
}
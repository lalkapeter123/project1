function username_empty() {
  if (document.getElementById("username").value == "< Enter Your User Name >") {
    document.getElementById("username").style.color = "Black";
    document.getElementById("username").value = "";  
  }
}

function username_empty_blur(){
  if (document.getElementById("username").value == "") {
    document.getElementById("username").style.color = "Gray";
    document.getElementById("username").value = "< Enter Your User Name >";  
  }
}

function showManagerProfile(){
    var p = document.getElementById("profileDiv");
    p.style.top="68pt";
    p.style.right="50pt";
    p.style.visibility="visible";
}

function hideManagerProfile(){
    var p = document.getElementById("profileDiv");
    p.style.visibility="hidden";
}

function editProfile(){
    window.location.href = './ManagerProfile.html';
}

function showEmployeeProfile(){
    var p = document.getElementById("profileDiv");
    p.style.top="68pt";
    p.style.right="50pt";
    p.style.visibility="visible";
}

function hideEmployeeProfile(){
    var p = document.getElementById("profileDiv");
    p.style.visibility="hidden";
}

function editProfile(){
    window.location.href = './EmployeeProfile.html';
}